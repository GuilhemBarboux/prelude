const Module = {
  preRun: [
    function() {
      Module.FS_createPreloadedFile(
        "/",
        "haarcascade_eye.xml",
        "https://raw.githubusercontent.com/opencv/opencv/master/data/haarcascades/haarcascade_eye.xml",
        true,
        false
      )
      Module.FS_createPreloadedFile(
        "/",
        "haarcascade_frontalface_default.xml",
        "https://raw.githubusercontent.com/opencv/opencv/master/data/haarcascades/haarcascade_frontalface_default.xml",
        true,
        false
      )
    },
  ],
  _main: function() {
    opencvIsReady()
  },
}

self.importScripts(
  "https://huningxin.github.io/opencv.js/build/asm.js/opencv.js"
)

/* Initialized */
let initialized = false
let height = 0
let width = 0
let classifierEyes
let classifierFace

function opencvIsReady() {
  console.log("OpenCV.js is ready for worker")
  classifierEyes = new cv.CascadeClassifier()
  classifierFace = new cv.CascadeClassifier()
  console.log(
    "worker init status eyes",
    classifierEyes.load("haarcascade_eye.xml")
  )
  console.log(
    "worker init status face",
    classifierFace.load("haarcascade_frontalface_default.xml")
  )
  initialized = true
  onInitializedTracking()
}

/* Worker Router */
self.onmessage = msg => {
  switch (msg.data.type) {
    case "init":
      width = msg.data.width
      height = msg.data.height
      onInitializedTracking()
      break
    case "tracking":
      onTracking(msg.data.imageData)
      break
    case "stop":
      break
  }
}

/* Tracking */
function onInitializedTracking() {
  if (!initialized || !width || !height) return
  self.postMessage({ type: "ready" })
}

function onTracking(imageData = []) {
  let isVisage = false
  let isOpenEyes = false
  let irisRadius = [0, 0]

  // OpenCV
  let result = {
    eyes: [],
    faces: [],
  }
  let buffer = imageData.buffer
  let mat = new cv.Mat(height, width, cv.CV_8UC4)
  mat.data.set(new Uint8Array(buffer))
  cv.cvtColor(mat, mat, cv.COLOR_RGBA2GRAY)
  let faces = new cv.RectVector()
  classifierFace.detectMultiScale(
    mat,
    faces,
    1.1,
    3,
    0,
    { width: 0, height: 0 },
    { width: 0, height: 0 }
  )
  console.log("worker detect " + faces)

  if (faces.size() > 0) {
    let eyes = new cv.RectVector()
    classifierEyes.detectMultiScale(
      mat,
      eyes,
      1.1,
      3,
      0,
      { width: 0, height: 0 },
      { width: 0, height: 0 }
    )
    for (let i = 0; i < faces.size(); i++) {
      const face = faces.get(i)

      result.faces.push(face)

      for (let i = 0; i < eyes.size(); i++) {
        const eye = eyes.get(i)
        if (
          eye.y > face.y + face.height / 2 &&
          eye.y < face.y + face.height &&
          eye.x > face.x + face.width / 2 &&
          eye.x < face.x + face.width
        ) {
          result.eyes.push(eye)
        }
      }
    }

    eyes.delete()
  }

  mat.delete()
  faces.delete()

  // Result
  self.postMessage({
    type: "result",
    isVisage,
    isOpenEyes,
    irisRadius,
    result,
  })
}
