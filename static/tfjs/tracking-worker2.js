const Module = {}

self.importScripts("https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-core")
self.importScripts("https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-converter")
self.importScripts(
  "https://cdn.jsdelivr.net/npm/@tensorflow-models/face-landmarks-detection"
)
self.importScripts(
  "https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-backend-webgl"
)

/* Initialized */
let debug = false
let initialized = false
let height = 0
let width = 0
let faceModel
let brightness = 0
let blinkModel

const isLoaded = faceLandmarksDetection
  .load(faceLandmarksDetection.SupportedPackages.mediapipeFacemesh, {
    shouldLoadIrisModel: false,
    maxFaces: 1,
    iouThreshold: 1,
  })
  .then(m => {
    const fallback = tf.getBackend()
    try {
      tf.setBackend("webgl")
    } catch (e) {
      tf.setBackend(fallback)
    }
    faceModel = m
  })

self.onmessage = msg => {
  /* Worker Router */
  switch (msg.data.type) {
    case "init":
      debug = msg.data.debug
      width = msg.data.width
      height = msg.data.height
      return onInitializedTracking()
      break
    case "tracking":
      return onTracking(msg.data.imageData, msg.data.config)
      break
    case "stop":
      break
  }
}

/* Tracking */
async function onInitializedTracking() {
  if (!width || !height) return
  blinkModel = await tf.loadGraphModel(
    "https://raw.githubusercontent.com/mirrory-dev/eyeblink/master/models/model.json"
  )
  await isLoaded
  self.postMessage({ type: "ready" })
}

function extractEyeBoundingBox(face, [top, right, bottom, left]) {
  const eyeTopY = face.scaledMesh[top][1]
  const eyeRightX = face.scaledMesh[right][0]
  const eyeBottomY = face.scaledMesh[bottom][1]
  const eyeLeftX = face.scaledMesh[left][0]
  const topLeft = [eyeLeftX, eyeTopY]
  const bottomRight = [eyeRightX, eyeBottomY]
  return { topLeft, bottomRight }
}

async function getPredictionWithinBoundingBox(input, boundingBoxes) {
  const prediction = tf.tidy(() => {
    const boundingBoxesNormalized = boundingBoxes.map(box => {
      return [
        box.topLeft[1] / input.shape[0],
        box.topLeft[0] / input.shape[1],
        box.bottomRight[1] / input.shape[0],
        box.bottomRight[0] / input.shape[1],
      ]
    })

    const cropped = tf.image
      .cropAndResize(
        input.expandDims(0),
        boundingBoxesNormalized,
        boundingBoxesNormalized.map(() => 0),
        [26, 34]
      )
      .toFloat()
    const grayscale = cropped.mean(3).expandDims(3)
    const inputImage = grayscale.toFloat().div(255)
    return blinkModel.predict(inputImage)
  })
  const result = await prediction.data()
  prediction.dispose()
  return result
}

async function onTracking(imageData = [], config = {}) {
  let isVisage = false
  let isOpenEyes = true
  let irisRadius = [1, 1]
  let limit = config.limit || 0.2

  const startTime = new Date().getTime()

  if (debug) console.log("Start", startTime)

  const tensor = tf.browser.fromPixels(imageData)
  const facePredictions = await faceModel.estimateFaces({
    input: tensor,
    predictIrises: false,
  })

  if (facePredictions.length > 0) {
    const face = facePredictions[0]

    const [topX] = face.annotations["midwayBetweenEyes"][0]
    const [rightX] = face.annotations["rightCheek"][0]
    const [leftX] = face.annotations["leftCheek"][0]
    const rotation = Math.abs((leftX - topX) / (topX - rightX))

    if (rotation > 0.6 && rotation < 1.4) {
      isVisage = true
      const rightEyeMeshIdx = [27, 243, 23, 130]
      const leftEyeMeshIdx = [257, 359, 253, 362]
      const rightEyeBB = extractEyeBoundingBox(face, rightEyeMeshIdx)
      const leftEyeBB = extractEyeBoundingBox(face, leftEyeMeshIdx)
      irisRadius = await this.getPredictionWithinBoundingBox(tensor, [
        leftEyeBB,
        rightEyeBB,
      ])

      isOpenEyes = irisRadius[0] > limit || irisRadius[1] > limit
    }
  }

  tensor.dispose()

  if (debug) console.log("Detection", new Date().getTime() - startTime, "ms")

  // Result
  self.postMessage({
    type: "result",
    isVisage,
    isOpenEyes,
    irisRadius,
    /*...(debug
      ? {
          thresholds: raws.map(t =>
            resizeImageData(t, 200, (200 * t.height) / t.width)
          ),
          limit,
        }
      : {}),*/
  })
}
