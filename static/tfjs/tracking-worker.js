// TODO : LIBS
function nearestNeighbor(src, dst) {
  let pos = 0

  for (let y = 0; y < dst.height; y++) {
    for (let x = 0; x < dst.width; x++) {
      const srcX = Math.floor((x * src.width) / dst.width)
      const srcY = Math.floor((y * src.height) / dst.height)

      let srcPos = (srcY * src.width + srcX) * 4

      dst.data[pos++] = src.data[srcPos++] // R
      dst.data[pos++] = src.data[srcPos++] // G
      dst.data[pos++] = src.data[srcPos++] // B
      dst.data[pos++] = src.data[srcPos++] // A
    }
  }
}

function bilinearInterpolation(src, dst) {
  function interpolate(k, kMin, kMax, vMin, vMax) {
    return Math.round((k - kMin) * vMax + (kMax - k) * vMin)
  }

  function interpolateHorizontal(offset, x, y, xMin, xMax) {
    const vMin = src.data[(y * src.width + xMin) * 4 + offset]
    if (xMin === xMax) return vMin

    const vMax = src.data[(y * src.width + xMax) * 4 + offset]
    return interpolate(x, xMin, xMax, vMin, vMax)
  }

  function interpolateVertical(offset, x, xMin, xMax, y, yMin, yMax) {
    const vMin = interpolateHorizontal(offset, x, yMin, xMin, xMax)
    if (yMin === yMax) return vMin

    const vMax = interpolateHorizontal(offset, x, yMax, xMin, xMax)
    return interpolate(y, yMin, yMax, vMin, vMax)
  }

  let pos = 0

  for (let y = 0; y < dst.height; y++) {
    for (let x = 0; x < dst.width; x++) {
      const srcX = (x * src.width) / dst.width
      const srcY = (y * src.height) / dst.height

      const xMin = Math.floor(srcX)
      const yMin = Math.floor(srcY)

      const xMax = Math.min(Math.ceil(srcX), src.width - 1)
      const yMax = Math.min(Math.ceil(srcY), src.height - 1)

      dst.data[pos++] = interpolateVertical(
        0,
        srcX,
        xMin,
        xMax,
        srcY,
        yMin,
        yMax
      ) // R
      dst.data[pos++] = interpolateVertical(
        1,
        srcX,
        xMin,
        xMax,
        srcY,
        yMin,
        yMax
      ) // G
      dst.data[pos++] = interpolateVertical(
        2,
        srcX,
        xMin,
        xMax,
        srcY,
        yMin,
        yMax
      ) // B
      dst.data[pos++] = interpolateVertical(
        3,
        srcX,
        xMin,
        xMax,
        srcY,
        yMin,
        yMax
      ) // A
    }
  }
}

function resizeImageData(image, width, height, algorithm) {
  algorithm = algorithm || "biliniear-interpolation"

  let resize
  switch (algorithm) {
    case "nearest-neighbor":
      resize = nearestNeighbor
      break
    case "biliniear-interpolation":
      resize = bilinearInterpolation
      break
    default:
      throw new Error(`Unknown algorithm: ${algorithm}`)
  }

  const result = new ImageData(width, height)

  resize(image, result)

  return result
}

function rgb2luma(px, pos) {
  return px[pos] * 0.299 + px[pos + 1] * 0.587 + px[pos + 2] * 0.114
}

const CanvasFilters = {
  grayscale(pixels, contrast, brightness) {
    // https://en.wikipedia.org/wiki/Relative_luminance
    const d = pixels.data
    let i = 0
    for (i = 0; i < d.length; i += 4) {
      const r = d[i + 0]
      const g = d[i + 1]
      const b = d[i + 2]
      const a = d[i + 3]
      let v = 0.2126 * r + 0.7152 * g + 0.0722 * b

      if (contrast && brightness) {
        v /= a

        // Apply contrast.
        v = (v - 0.5) * Math.max(contrast, 0) + 0.5

        // Apply brightness.
        v += brightness

        // Return final pixel color.
        v *= a
      }

      d[i + 0] = d[i + 1] = d[i + 2] = v
    }
    return pixels
  },

  threshold(pixels, threshold) {
    const d = pixels.data
    let i = 0
    for (i = 0; i < d.length; i += 4) {
      const r = d[i + 0]
      const g = d[i + 1]
      const b = d[i + 2]
      let v = 0
      if (r + g + b > threshold) {
        v = 255
      }
      d[i + 0] = d[i + 1] = d[i + 2] = v
    }
    return pixels
  },

  adjusted(pixels, thold = 0) {
    let adjwidth = pixels.width,
      idata,
      data,
      i,
      min = -1,
      max = -1, // to find min-max
      maxH = 0, // to find scale of histogram
      scale,
      hgram = new Uint32Array(adjwidth) // histogram buffer (or use Float32)

    // get image data
    idata = pixels.data
    data = idata.data // the bitmap itself

    // get lumas and build histogram
    for (i = 0; i < data.length; i += 4) {
      let luma = Math.round(rgb2luma(data, i))
      hgram[luma]++ // add to the luma bar (and why we need an integer)
    }

    // find tallest bar so we can use that to scale threshold
    for (i = 0; i < adjwidth; i++) {
      if (hgram[i] > maxH) maxH = hgram[i]
    }

    // use that for threshold
    thold *= maxH

    // find min value
    for (i = 0; i < adjwidth * 0.5; i++) {
      if (hgram[i] > thold) {
        min = i
        break
      }
    }
    if (min < 0) min = 0 // not found, set to default 0

    // find max value
    for (i = adjwidth - 1; i > adjwidth * 0.5; i--) {
      if (hgram[i] > thold) {
        max = i
        break
      }
    }
    if (max < 0) max = 255 // not found, set to default 255

    scale = (255 / (max - min)) * 2 // x2 compensates (play with value)

    // scale all pixels
    for (i = 0; i < data.length; i += 4) {
      data[i] = Math.max(0, data[i] - min) * scale
      data[i + 1] = Math.max(0, data[i + 1] - min) * scale
      data[i + 2] = Math.max(0, data[i + 2] - min) * scale
    }

    return pixels
  },
}

// END TODO

const Module = {}

self.importScripts("https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-core")
self.importScripts("https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-converter")
self.importScripts("https://cdn.jsdelivr.net/npm/@tensorflow-models/facemesh")
self.importScripts(
  "https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-backend-webgl"
)

/* Initialized */
let debug = false
let initialized = false
let height = 0
let width = 0
let model
let brightness = 0

const isLoaded = facemesh
  .load({
    maxContinuousChecks: 5,
    detectionConfidence: 0.9,
    maxFaces: 1,
    iouThreshold: 0.3,
    scoreThreshold: 0.75,
  })
  .then(m => {
    const fallback = tf.getBackend()
    try {
      tf.setBackend("webgl")
    } catch (e) {
      tf.setBackend(fallback)
    }
    model = m
  })

self.onmessage = msg => {
  /* Worker Router */
  switch (msg.data.type) {
    case "init":
      debug = msg.data.debug
      width = msg.data.width
      height = msg.data.height
      return onInitializedTracking()
      break
    case "tracking":
      return onTracking(msg.data.imageData, msg.data.config)
      break
    case "stop":
      break
  }
}

/* Tracking */
async function onInitializedTracking() {
  if (!width || !height) return
  await isLoaded
  self.postMessage({ type: "ready" })
}

async function onTracking(imageData = [], config = {}) {
  let isVisage = false
  let isOpenEyes = false
  let irisRadius = [0, 0]
  let result = null
  let params = Object.assign(
    {
      threshold: 0.3,
      contrast: 0.3,
      brightness: 0.5,
    },
    config
  )
  let limit = config.limit || 1

  // Auto brightness
  /*let luma = 0
  for (let i = 0; i < imageData.data.length; i += 4) {
    luma += Math.round(rgb2luma(imageData.data, i))
  }
  brightness = luma / imageData.data.length / 4*/

  // TFJS
  const predictions = await model.estimateFaces(imageData)
  const thresholds = []
  const raws = []

  if (predictions.length > 0) {
    result = predictions[0]

    isVisage = true

    // Process
    const eyes = []

    // Left
    const ly = [
      ...result.annotations.leftEyeUpper1.map(v => v[1]),
      ...result.annotations.leftEyeUpper0.map(v => v[1]),
    ]

    eyes[0] = {
      max: [
        Math.round(
          Math.max.apply(
            null,
            result.annotations.leftEyeUpper1.map(v => v[0])
          )
        ),
        Math.round(Math.max.apply(null, ly)),
      ],
      min: [
        Math.round(
          Math.min.apply(
            null,
            result.annotations.leftEyeUpper0.map(v => v[0])
          )
        ),
        Math.round(Math.min.apply(null, ly)),
      ],
    }

    // right
    const ry = [
      ...result.annotations.rightEyeUpper1.map(v => v[1]),
      ...result.annotations.rightEyeUpper0.map(v => v[1]),
    ]

    eyes[1] = {
      max: [
        Math.round(
          Math.max.apply(
            null,
            result.annotations.rightEyeUpper1.map(v => v[0])
          )
        ),
        Math.round(Math.max.apply(null, ry)),
      ],
      min: [
        Math.round(
          Math.min.apply(
            null,
            result.annotations.rightEyeUpper0.map(v => v[0])
          )
        ),
        Math.round(Math.min.apply(null, ry)),
      ],
    }

    eyes.forEach((e, side) => {
      const eyeW = e.max[0] - e.min[0]
      const eyeH = e.max[1] - e.min[1]
      const eyeData = new Uint8ClampedArray(eyeW * eyeH * 4)

      let index = 0
      for (let y = 0; y < height; y++) {
        for (let x = 0; x < width; x++) {
          if (x >= e.min[0] && x < e.max[0] && y >= e.min[1] && y < e.max[1]) {
            const pos = (y * width + x) * 4
            eyeData[index] = imageData.data[pos]
            eyeData[index + 1] = imageData.data[pos + 1]
            eyeData[index + 2] = imageData.data[pos + 2]
            eyeData[index + 3] = imageData.data[pos + 3]
            index += 4
          }
        }
      }

      raws[side] = new ImageData(eyeData, eyeW, eyeH)
      const adjust = CanvasFilters.grayscale(raws[side])
      /*const grayscale = CanvasFilters.grayscale(
        raws[side],
        10 * params.contrast,
        params.brightness
      )*/
      thresholds[side] = CanvasFilters.threshold(adjust, 255 * params.threshold)

      // Count % of eyes
      let count = 0
      for (let i = 0, l = raws[side].data.length; i < l; i += 4) {
        if (raws[side].data[i] > 0) count++
      }

      irisRadius[side] = count / (eyeW * eyeH)
    })

    if (irisRadius[0] > limit && irisRadius[1] > limit) {
      isOpenEyes = true
    }
  }

  // Result
  self.postMessage({
    type: "result",
    isVisage,
    isOpenEyes,
    irisRadius,
    ...(debug
      ? {
          thresholds: raws.map(t =>
            resizeImageData(t, 200, (200 * t.height) / t.width)
          ),
          limit,
        }
      : {}),
  })
}
