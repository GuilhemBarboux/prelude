import React from "react"
import AudioManager from "./AudioManager"

const withAudio = Component => props => {
  return (
    <AudioManager>
      <Component {...props} />
    </AudioManager>
  )
}

export default withAudio
