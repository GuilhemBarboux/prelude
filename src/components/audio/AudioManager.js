import React, { createContext, useCallback, useEffect, useRef } from "react"

export const AudioContext = createContext({
  audioContext: {},
  registerTrack: () => {},
  playTrack: () => {},

  getAudioValue: () => {},
  connect: () => {},
})

const AudioManager = ({ children, audioDataSize = 64 }) => {
  const tracks = useRef([])
  const contextAudio = useRef(
    new (window.AudioContext || window.webkitAudioContext)()
  )
  const analyser = useRef(contextAudio.current.createAnalyser())
  const audioData = useRef(new Float32Array(audioDataSize))

  // States
  const registerTrack = useCallback((src, filters = []) => {
    if (!tracks.current[src]) {
      const audio = new Audio()

      audio.src = src
      audio.load()

      const track = {
        audio,
        source: contextAudio.current.createMediaElementSource(audio),
        filters,
        play: async (src, volume, loop = false, delay = 0) => {
          audio.currentTime = delay
          audio.volume = volume
          audio.loop = loop

          await contextAudio.current.resume()
          await audio.play(delay)
        },
        setVolume: volume => {
          audio.volume = volume
        },
      }

      const sources = [track.source, ...filters]

      sources
        .filter(s => typeof s.connect === "function")
        .forEach((s, i) => {
          if (i > 0) sources[i - 1].connect(s)
          if (i === sources.length - 1) s.connect(analyser.current)
        })

      tracks.current[src] = track

      return track
    } else {
      return tracks.current[src]
    }
  }, [])

  const playTrack = useCallback(
    async (src, volume, loop = false, delay = 0) => {
      let track = registerTrack(src)

      track.audio.currentTime = delay
      track.audio.volume = volume
      track.audio.loop = loop

      await contextAudio.current.resume()
      await track.audio.play(delay)

      return track
    },
    [registerTrack]
  )

  const getAudioValue = useCallback(() => {
    analyser.current.getFloatFrequencyData(audioData.current)

    return (
      audioData.current.reduce(
        (a, b) => 256 + Math.max(audioData.current[1], -256)
      ) / 256
    )
  }, [])

  // Initialze
  useEffect(() => {
    analyser.current.fftSize = audioData.current.length
    analyser.current.connect(contextAudio.current.destination)

    return () => {
      analyser.current.disconnect(contextAudio.current.destination)
    }
  }, [])

  return (
    <AudioContext.Provider
      value={{
        audioContext: contextAudio.current,
        registerTrack,
        playTrack,
        getAudioValue,
      }}
    >
      {children}
    </AudioContext.Provider>
  )
}

AudioManager.propTypes = {}

export default AudioManager
