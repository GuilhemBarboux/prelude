import React, { createContext } from "react"

export default createContext({
  addUpdate: () => {},
  removeUpdate: () => {},

  addResize: () => {},
  removeResize: () => {},

  addObject: () => {},
  removeObject: () => {},

  fonts: {},
  getFont: () => {},

  setBox: () => {},
  setDeformation: () => {},
})
