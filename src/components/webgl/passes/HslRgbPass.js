import { ShaderPass } from "three/examples/jsm/postprocessing/ShaderPass"
import Vertex from "./shaders/base.vert.glsl"
import Fragment from "./shaders/hslrgb.frag.glsl"
import { Vector2 } from "three"

const HslRgbShader = {
  uniforms: {
    tDiffuse: { value: null },
    resolution: { value: new Vector2() },
  },
  vertexShader: Vertex,
  fragmentShader: Fragment,
}

export default class HslRgbPass extends ShaderPass {
  constructor() {
    super(HslRgbShader)
  }
  setResolution(w, h) {
    this.uniforms.resolution.value = new Vector2(w, h)
  }
}
