import { ShaderPass } from "three/examples/jsm/postprocessing/ShaderPass"
import Vertex from "./shaders/base.vert.glsl"
import Fragment from "./shaders/blur.frag.glsl"
import { Vector2 } from "three"

const BlurShader = {
  uniforms: {
    tDiffuse: { value: null },
    resolution: { value: new Vector2() },
  },
  vertexShader: Vertex,
  fragmentShader: Fragment,
}

export default class BlurPass extends ShaderPass {
  constructor() {
    super(BlurShader)
  }
  setResolution(w, h) {
    this.uniforms.resolution.value = new Vector2(w, h)
  }
}
