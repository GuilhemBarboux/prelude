import { ShaderPass } from "three/examples/jsm/postprocessing/ShaderPass"
import Vertex from "./shaders/base.vert.glsl"
import Fragment from "./shaders/estampe.frag.glsl"
import { Vector2 } from "three"

const EstampeShader = {
  uniforms: {
    tDiffuse: { value: null },
    resolution: { value: new Vector2() },
  },
  vertexShader: Vertex,
  fragmentShader: Fragment,
}

export default class EstampePass extends ShaderPass {
  constructor() {
    super(EstampeShader)
  }
  setResolution(w, h) {
    this.uniforms.resolution.value = new Vector2(w, h)
  }
}
