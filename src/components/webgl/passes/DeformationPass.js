import { ShaderPass } from "three/examples/jsm/postprocessing/ShaderPass"
import Vertex from "./shaders/base.vert.glsl"
import Fragment from "./shaders/deformation.frag.glsl"
import {
  DataTexture,
  FloatType,
  LinearFilter,
  LuminanceFormat,
  Vector2,
} from "three"

const DeformationShader = {
  uniforms: {
    tDiffuse: { value: null },
    resolution: { value: new Vector2() },
    uStep: { value: null },
  },
  vertexShader: Vertex,
  fragmentShader: Fragment,
}

export default class DeformationPass extends ShaderPass {
  constructor(length = 64) {
    super(DeformationShader)

    this.data = new Float32Array(length || 2)
    this.dataTexture = new DataTexture(
      this.data,
      this.data.length,
      1,
      LuminanceFormat,
      FloatType
    )
    this.dataTexture.magFilter = LinearFilter
    this.dataTexture.minFilter = LinearFilter
    this.dataTexture.needsUpdate = true

    // Params
    this.uniforms.uStep.value = this.dataTexture
  }
  setResolution(w, h) {
    this.uniforms.resolution.value = new Vector2(w, h)
  }
  addDeformation(data = 0.0) {
    for (let i = this.dataTexture.image.data.length - 1; i > 0; i--) {
      this.dataTexture.image.data[i] = this.dataTexture.image.data[i - 1]
    }
    this.dataTexture.image.data[0] =
      data + this.dataTexture.image.data[1] * 0.02

    this.dataTexture.needsUpdate = true
  }
}
