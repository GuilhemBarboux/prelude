#ifdef GL_ES
precision mediump float;// highp if OpenGL
#endif

#ifndef saturate
#define saturate(v) clamp(v, 0.0, 1.0);
#endif

varying vec2 vUv;
uniform sampler2D tDiffuse;
uniform vec2 resolution;

vec3 hue_to_rgb(float hue) {
    float R = abs(hue * 6.0 - 3.0) - 1.0;
    float G = 2.0 - abs(hue * 6.0 - 2.0);
    float B = 2.0 - abs(hue * 6.0 - 4.0);
    return saturate(vec3(R, G, B));
}

vec3 hsl_to_rgb(vec3 hsl) {
    vec3 rgb = hue_to_rgb(hsl.x);
    float C = (1.0 - abs(2.0 * hsl.z - 1.0)) * hsl.y;
    return (rgb - 0.5) * C + hsl.z;
}

void main() {
    vec4 texture = texture2D(tDiffuse, vUv);
    vec3 color = hsl_to_rgb(texture.rgb);
    gl_FragColor = vec4(color, 1.0);
}
