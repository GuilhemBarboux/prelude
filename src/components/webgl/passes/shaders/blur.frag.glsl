#ifdef GL_ES
precision mediump float; // highp if OpenGL
#endif

varying vec2 vUv;
uniform sampler2D tDiffuse;
uniform vec2 resolution;

vec4 blur13(sampler2D image, vec2 uv, vec2 resolution, vec2 direction) {
  vec4 color = vec4(0.0);
  vec2 off1 = vec2(1.411764705882353) * direction;
  vec2 off2 = vec2(3.2941176470588234) * direction;
  vec2 off3 = vec2(5.176470588235294) * direction;
  color += texture2D(image, uv) * 0.1964825501511404;
  color += texture2D(image, uv + (off1 / resolution)) * 0.2969069646728344;
  color += texture2D(image, uv - (off1 / resolution)) * 0.2969069646728344;
  color += texture2D(image, uv + (off2 / resolution)) * 0.09447039785044732;
  color += texture2D(image, uv - (off2 / resolution)) * 0.09447039785044732;
  color += texture2D(image, uv + (off3 / resolution)) * 0.010381362401148057;
  color += texture2D(image, uv - (off3 / resolution)) * 0.010381362401148057;
  return color;
}

void main() {
  float force = 2.0;
  float r = blur13(tDiffuse, vUv, resolution, vec2(1.5 * force, 0.0)).r;
  float g = blur13(tDiffuse, vUv, resolution, vec2(0.1 * force, 0.0)).g;
  float b = blur13(tDiffuse, vUv, resolution, vec2(1.0 * force, 0.0)).b;

  gl_FragColor = vec4(r, g, b, 1.0);
}
