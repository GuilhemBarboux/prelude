#ifdef GL_ES
precision mediump float;// highp if OpenGL
#endif

varying vec2 vUv;
uniform sampler2D tDiffuse;
uniform vec2 resolution;
uniform float time;
uniform vec4 box;

float random (vec2 st) {
    return fract(sin(dot(st.xy,
    vec2(12.9898, 78.233)))*
    43758.5453123);
}

void main() {
    vec3 color = texture2D(tDiffuse, vUv).rgb;
    float hover = 0.03;

    if (
    vUv.x * resolution.x >= box.x && vUv.x * resolution.x <= box.x + box.z &&
    vUv.y * resolution.y >= box.y && vUv.y * resolution.y <= box.y + box.a
    ) {
        hover = 0.2;
    }

    if (mod(gl_FragCoord.x, 2.0) > 1.0 || mod(gl_FragCoord.y, 3.0) > 2.0) {
        color = mix(vec3(0.185 + random(vec2(mod(time * 0.01, 2.0), mod(time * 0.001, 2.0))) * hover), color, color.r);
    }

    gl_FragColor = vec4(color, 1.0);
}
