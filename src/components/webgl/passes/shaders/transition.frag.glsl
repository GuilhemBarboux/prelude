#ifdef GL_ES
precision mediump float; // highp if OpenGL
#endif

uniform sampler2D tDiffuse;
uniform float transition;

varying vec2 vUv;

void main() {
  float x = transition;
  float transition = fract(x + 0.5) * 2.0 - 1.0;
  vec2 uv = vec2(vUv.x, vUv.y - transition);
  vec4 texture = texture2D(tDiffuse, vUv);
  vec4 black = vec4(vec3(0.0), 1.0);

  if (vUv.y <= x * 2.0 && vUv.y >= x * 2.0 - 1.0) {
    gl_FragColor = black;
  } else {
    gl_FragColor = texture;
  }
}
