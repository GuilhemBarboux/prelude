#ifdef GL_ES
precision mediump float; // highp if OpenGL
#endif

varying vec2 vUv;
uniform sampler2D tDiffuse;
uniform vec2 resolution;

vec4 estampe(sampler2D image, vec2 uv, vec2 resolution) {
  float step = -1.0;
  vec2 uv1 = vec2(uv.x, uv.y);
  vec2 uv2 = vec2(uv.x + (step / resolution.x), uv.y);
  vec4 tex1 = texture2D(image, uv1);
  vec4 tex2 = vec4(vec3(1.0) - texture2D(image, uv2).rgb, 1.0);
  return mix((tex1 + tex2) / 2.0, tex1, 0.8);
}

void main() {
  gl_FragColor = estampe(tDiffuse, vUv, resolution);
}
