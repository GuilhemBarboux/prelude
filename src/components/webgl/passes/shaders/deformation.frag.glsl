#ifdef GL_ES
precision mediump float; // highp if OpenGL
#endif
const int size = 16;

varying vec2 vUv;
uniform sampler2D tDiffuse;
uniform sampler2D uStep;
uniform vec2 resolution;

float gain(float x, float k) {
    float a = 0.5*pow(2.0*((x<0.5)?x:1.0-x), k);
    return (x<0.5)?a:1.0-a;
}

float impulse(float k, float x) {
    float h = k*x;
    return h*exp(1.0-h);
}

void main() {
  float level = 0.8;
  vec4 texture = texture2D(uStep, vec2(vUv.x, 0.0));
  float s = step(level, texture.r) * (texture.r - level) * 0.15;
  vec2 uv = vec2(vUv.x, vUv.y + smoothstep(0.0, 1.0 - level, s));
  gl_FragColor = texture2D(tDiffuse, uv);
}
