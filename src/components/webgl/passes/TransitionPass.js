import { ShaderPass } from "three/examples/jsm/postprocessing/ShaderPass"
import Vertex from "./shaders/base.vert.glsl"
import Fragment from "./shaders/transition.frag.glsl"

const TransitionShader = {
  uniforms: {
    tDiffuse: { value: null },
    transition: { value: 0.0 },
  },
  vertexShader: Vertex,
  fragmentShader: Fragment,
}

export default class TransitionPass extends ShaderPass {
  constructor() {
    super(TransitionShader)
  }
  setTransition(transition = 0) {
    this.uniforms.transition.value = Math.max(Math.min(transition, 1.0), 0.0)
  }
}
