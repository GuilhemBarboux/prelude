import { ShaderPass } from "three/examples/jsm/postprocessing/ShaderPass"
import Vertex from "./shaders/base.vert.glsl"
import Fragment from "./shaders/grid.frag.glsl"
import { Vector2, Vector4 } from "three"

const GridShader = {
  uniforms: {
    tDiffuse: { value: null },
    resolution: { value: new Vector2() },
    box: { value: new Vector4() },
    time: { value: 0.0 },
  },
  vertexShader: Vertex,
  fragmentShader: Fragment,
}

export default class GridPass extends ShaderPass {
  constructor() {
    super(GridShader)
    this.startTime = Date.now()
  }
  setResolution(w, h) {
    this.uniforms.resolution.value = new Vector2(w, h)
  }
  setBox(box = new Vector4(0, 0, 0, 0)) {
    if (box instanceof Vector4) {
      this.uniforms.box.value = box
    }
  }
  updateTime() {
    this.uniforms.time.value = 0.001 * (Date.now() - this.startTime)
  }
}
