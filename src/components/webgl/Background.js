import React, { useEffect, useMemo, useRef, useState } from "react"
import styled from "@emotion/styled"

const CanvasWrapper = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;

  width: 100%;
  height: 100%;

  overflow: hidden;
`

const ContentWrapper = styled.div`
  position: relative;
  zindex: 1000;
`

const Background = ({ canvas, children }) => (
  <>
    <CanvasWrapper>{canvas}</CanvasWrapper>
    <ContentWrapper>{children}</ContentWrapper>
  </>
)

Background.propTypes = {}

export default Background
