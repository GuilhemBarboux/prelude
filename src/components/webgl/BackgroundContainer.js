import React, {
  useCallback,
  useEffect,
  useMemo,
  useReducer,
  useRef,
  useState,
} from "react"
import { FontLoader, Vector4 } from "three"
import BackgroundCanvas from "@src/components/webgl/BackgroundCanvas"
import BackgroundContext from "@src/components/webgl/BackgroundContext"
import Background from "@src/components/webgl/Background"

const fontLoader = new FontLoader()

const listenerReducer = (state, action) => {
  switch (action.type) {
    case "addListener":
      return { listeners: [...state.listeners, action.listener] }
    case "removeListener":
      return { listeners: state.listeners.filter(l => l != action.listener) }
    default:
      throw new Error()
  }
}

const sceneReducer = (state, action) => {
  switch (action.type) {
    case "addScene":
      return { objects: [action.object] }
    case "addObject":
      return { objects: [...state.objects, action.object] }
    case "removeObject":
      return { objects: state.objects.filter(o => o != action.object) }
    default:
      throw new Error()
  }
}

const fontReducer = (state, action) => {
  if (action.type === "addFont" && action.fontName && action.font) {
    return {
      fonts: {
        ...state.fonts,
        [action.fontName]: action.font,
      },
    }
  } else {
    throw new Error()
  }
}

const BackgroundContainer = props => {
  // Properties
  const { children, transition, deformation, update, fonts } = props

  // Refs
  const idRAF = useRef(null)

  // Stats
  const [box, setBox] = useState(new Vector4())

  // Updates
  const [updates, dispatchUpdate] = useReducer(listenerReducer, {
    listeners: [],
  })
  const addUpdate = useCallback(
    listener => dispatchUpdate({ type: "addListener", listener }),
    [dispatchUpdate]
  )
  const removeUpdate = useCallback(
    listener => dispatchUpdate({ type: "removeListener", listener }),
    [dispatchUpdate]
  )
  useEffect(() => {
    const globalUpdate = () => {
      updates.listeners.forEach(l => typeof l === "function" && l())
      idRAF.current = requestAnimationFrame(globalUpdate)
    }

    globalUpdate()

    return () => {
      cancelAnimationFrame(idRAF.current)
    }
  }, [updates])

  // Resizes
  const [resizes, dispatchResize] = useReducer(listenerReducer, {
    listeners: [],
  })
  const addResize = useCallback(
    listener => dispatchResize({ type: "addListener", listener }),
    [dispatchResize]
  )
  const removeResize = useCallback(
    listener => dispatchResize({ type: "removeListener", listener }),
    [dispatchResize]
  )
  useEffect(() => {
    const globalResize = () => {
      resizes.listeners.forEach(l => typeof l === "function" && l())
    }

    // TODO: add debounce
    window.addEventListener("resize", globalResize)

    return () => {
      window.removeEventListener("resize", globalResize)
    }
  }, [resizes])

  // Objects
  const [scene, dispatchScene] = useReducer(sceneReducer, { objects: [] })
  const addObject = useCallback(
    object => dispatchScene({ type: "addObject", object }),
    [dispatchScene]
  )
  const removeObject = useCallback(
    object => dispatchScene({ type: "removeObject", object }),
    [dispatchScene]
  )

  // Fonts
  const [fontStore] = useReducer(fontReducer, {
    fonts: Object.keys(fonts).reduce((result, name) => {
      const JSON = fonts[name]
      const font = fontLoader.parse(JSON)
      result[JSON.familyName] = font
      result[name] = font
      return result
    }, {}),
  })
  /*const addFont = useCallback(
    item => {
      dispatchFont({
        type: "addFont",
        fontName: item.name,
        font: item.font,
      })
    },
    [dispatchFont]
  )*/

  // Update
  useEffect(() => {
    addUpdate(update)
    return () => {
      removeUpdate(update)
    }
  }, [update])

  // Canvas
  const canvas = useMemo(
    () => (
      <BackgroundCanvas
        deformation={deformation}
        transition={transition}
        objects={scene.objects}
        box={box}
      />
    ),
    [deformation, transition, scene, box]
  )

  const value = useMemo(
    () => ({
      addResize,
      removeResize,
      addUpdate,
      removeUpdate,
      addObject,
      removeObject,
      fonts: fontStore.fonts,
      setBox,
    }),
    []
  )

  return (
    <BackgroundContext.Provider value={value}>
      <Background canvas={canvas}>{children}</Background>
    </BackgroundContext.Provider>
  )
}

BackgroundContainer.propTypes = {}

export default BackgroundContainer
