import { Object3D, Vector3 } from "three"
import Text from "@src/components/webgl/meshs/Text"
import { Plane } from "@src/components/webgl/meshs"

export default class TextAnimate extends Object3D {
  constructor(font, message, color) {
    super()
    this.text = new Text(font, message, color)
    this.width = 0
    this.size = 0
    this.offset = 0
    this.repeat = []

    this.position.z = 10
  }
  move(x, y, z) {
    this.position.copy(new Vector3(x, y, z))
  }
  resize(width, fontSize) {
    this.text.resize(fontSize)

    for (let i = this.children.length - 1; i >= 0; i--) {
      this.remove(this.children[i])
    }

    const bb = this.text.geometry.boundingBox
    const size = bb.max.x - bb.min.x + fontSize * 0.75

    this.repeat = []
    for (let i = 0, l = Math.ceil(width / size) + 1; i < l; i++) {
      const textRepeat = this.text.clone()
      this.add(textRepeat)
      textRepeat.position.x += size * i - width / 2
      this.repeat.push(textRepeat)
    }

    this.size = size
    this.width = width

    // Debug
    // const plane = new Plane(0x00ffff)
    // plane.resize(width, 50)
    // plane.position.z = -1
    // this.add(plane)
  }
  animate(delta = -1) {
    this.repeat.forEach((text, i) => {
      text.position.x += delta
      if (delta < 0 && text.position.x + this.size / 2 < -this.width / 2) {
        text.position.x += this.repeat.length * this.size
      } else if (text.position.x - this.size / 2 > this.width / 2) {
        text.position.x -= this.repeat.length * this.size
      }
    })
  }
}
