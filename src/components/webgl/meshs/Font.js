import * as THREE from "three"

function getXMid(align, boundingBox, width) {
  switch (align) {
    case "center":
      return -0.5 * (boundingBox.max.x - boundingBox.min.x) // center horizontally
    case "left":
      return -width / 2
    default:
      return -width / 2 + Number(align)
  }
}

function getYMid(align, boundingBox, height, fontSize) {
  switch (align) {
    case "center":
      return 0.5 * (boundingBox.max.y - boundingBox.min.y) - fontSize
    case "top":
      return height / 2 - fontSize
    default:
      return height / 2 - fontSize - Number(align)
  }
}

function generateGeometry(
  font,
  message,
  width,
  height,
  fontSize,
  xAlign,
  yAlign,
  withTranslate = true
) {
  const shapes = font.generateShapes(message, fontSize) // size of a letter in pixel
  const geometry = new THREE.ShapeGeometry(shapes)

  geometry.computeBoundingBox()

  let xMid = getXMid(xAlign, geometry.boundingBox, width)
  let yMid = getYMid(yAlign, geometry.boundingBox, height, fontSize)

  if (withTranslate) geometry.translate(xMid, yMid, 0)
  return geometry
}

export default class Font extends THREE.Mesh {
  constructor(
    jsonFont,
    message,
    width,
    height,
    properties = { x: "center", y: "center", fontSize: 12, color: 0x000000 }
  ) {
    const loader = new THREE.FontLoader()
    const alpha = { opacity: 1, transparent: false }
    if (properties.transparent) {
      alpha.opacity = 0.25
      alpha.transparent = true
    }
    if (properties.letterSpacing)
      jsonFont.boundingBox.yMax = properties.letterSpacing
    const font = loader.parse(jsonFont)
    const matLite = new THREE.MeshBasicMaterial({
      color: properties.color,
      opacity: alpha.opacity,
      transparent: alpha.transparent,
    })
    const geometry = generateGeometry(
      font,
      message,
      width,
      height,
      properties.fontSize,
      properties.x,
      properties.y,
      false
    )

    super(geometry, matLite)
    this.oldWidth = width
    this.oldHeight = height
    this.font = font
    this.message = message
    this.properties = properties
  }
  resize(w, h) {
    const fontSize = (this.properties.fontSize * w) / this.oldWidth // works if ratio means something...
    this.geometry = generateGeometry(
      this.font,
      this.message,
      w,
      h,
      fontSize,
      this.properties.x,
      this.properties.y,
      true
    )
  }
}
