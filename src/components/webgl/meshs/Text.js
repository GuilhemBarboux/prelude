import { Mesh, MeshBasicMaterial, ShapeGeometry, Vector3 } from "three"

export default class Text extends Mesh {
  constructor(font, message, color) {
    const material = new MeshBasicMaterial({ color })
    const geometry = new ShapeGeometry(font.generateShapes(message, 200))

    super(geometry, material)

    this.font = font
    this.message = message
    this.color = color
  }
  clone() {
    return new Mesh(this.geometry, this.material)
  }
  resize(fontSize) {
    this.geometry = new ShapeGeometry(
      this.font.generateShapes(this.message, fontSize)
    )

    this.geometry.computeBoundingBox()

    const boundingBox = this.geometry.boundingBox

    const translate = {
      x: -0.5 * (boundingBox.max.x - boundingBox.min.x),
      y: -0.5 * fontSize,
    }

    this.geometry.translate(translate.x, translate.y, 0)
  }
  move(x, y, z) {
    this.position.copy(new Vector3(x, y, z))
  }
  animate(o) {
    this.material.transparent = true
    this.material.opacity = o
  }
}
