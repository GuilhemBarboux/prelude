import { Mesh, MeshBasicMaterial, PlaneGeometry, Texture, Vector3 } from "three"

export default class PlaneImage extends Mesh {
  constructor(src, color, fixed = true, pivotV = "center", pivotH = "center") {
    const texture = new Texture()
    const image = new Image()

    // Create mesh
    const material = new MeshBasicMaterial({ map: texture, color })
    const geometry = new PlaneGeometry(1, 1, 1, 1)
    super(geometry, material)

    // Ratio
    this.fixed = fixed
    this.ratio = 1
    this.width = -1
    this.height = -1
    this.pivotV = pivotV
    this.pivotH = pivotH
    this.origin = new Vector3()

    // Load Image
    texture.image = image
    image.onload = () => {
      this.ratio = image.height / image.width
      this.resize(this.width, this.height)
      this.move(this.origin.x, this.origin.y, this.origin.z)
      texture.needsUpdate = true
    }
    image.src = src
  }
  resize(w = -1, h = -1) {
    this.width = w
    this.height = h

    if (this.fixed) {
      if (w >= 0) {
        this.scale.set(w, w * this.ratio, 1.0)
      } else {
        this.scale.set(h / this.ratio, h, 1.0)
      }
    } else {
      if (w * this.ratio >= h) {
        this.scale.set(w, w * this.ratio, 1.0)
      } else {
        this.scale.set(h / this.ratio, h, 1.0)
      }
    }
  }
  move(x, y, z) {
    this.origin = new Vector3(x, y, z)

    this.position.copy(this.origin)

    switch (this.pivotV) {
      case "top":
        this.position.setY(this.origin.y - this.scale.y / 2)
        break
      case "bottom":
        this.position.setY(this.origin.y + this.scale.y / 2)
        break
      default:
        break
    }
  }
  pivot(v = "center", h = "center") {
    this.pivotV = v
    this.pivotH = h
  }
}
