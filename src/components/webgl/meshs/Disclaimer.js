import {
  Mesh,
  MeshBasicMaterial,
  PlaneGeometry,
  Vector3,
  RepeatWrapping,
  Texture,
} from "three"
import eyesSrc from "@src/assets/images/sprites/eyes.png"
import eyesWhiteSrc from "@src/assets/images/sprites/eyes2.png"
import eyesAnim from "@src/assets/images/sprites/eyes.json"

const totalRadius = 1
const frames = [1, 211, 421, 631, 841]
const spriteSize = 1050

export default class Disclaimer extends Mesh {
  constructor(auto = true, white = false) {
    const texture = new Texture()
    const image = new Image()
    const material = new MeshBasicMaterial({ map: texture })
    const geometry = new PlaneGeometry(1, 1, 1, 1)

    texture.wrapS = texture.wrapT = RepeatWrapping
    texture.repeat.set(1 / 5, 1)

    texture.image = image
    image.onload = () => {
      this.resize(this.width, this.height)
      this.move(this.origin.x, this.origin.y, this.origin.z)
      texture.needsUpdate = true
    }
    image.src = white ? eyesWhiteSrc : eyesSrc

    super(geometry, material)

    this.width = -1
    this.height = -1
    this.origin = new Vector3()
    this.texture = texture
    this.radius = 1

    // Animation
    this.auto = auto
    this.now = Date.now()
    this.then = Date.now()
    this.scaleDuration = eyesAnim.meta.scale * 150
    this.currentFrame = 0
    this.frames = eyesAnim.frames
  }
  resize(w, h) {
    this.width = w
    this.height = h
    this.scale.set(w, h, 1.0)
  }
  move(x, y, z) {
    this.origin = new Vector3(x, y, z)
    this.position.copy(new Vector3(x, y, z))
  }
  closure(radius) {
    this.auto = false
    this.radius = radius
  }
  animate() {
    if (this.auto) {
      this.now = Date.now()

      const delta = this.now - this.then
      if (delta > this.scaleDuration) {
        this.then = this.now - (delta % this.scaleDuration)
        this.currentFrame++
        if (this.currentFrame === this.frames.length) {
          this.currentFrame = 0
        }
        this.texture.offset.x =
          this.frames[this.currentFrame].frame.x / spriteSize
      }
    } else {
      if (this.radius > totalRadius * 0.9) {
        this.texture.offset.x = frames[0] / spriteSize
      } else if (this.radius > totalRadius * 0.7) {
        this.texture.offset.x = frames[3] / spriteSize
      } else if (this.radius > totalRadius * 0.5) {
        this.texture.offset.x = frames[1] / spriteSize
      } else if (this.radius > totalRadius * 0.1) {
        this.texture.offset.x = frames[4] / spriteSize
      } else {
        this.texture.offset.x = frames[2] / spriteSize
      }
    }
  }
}
