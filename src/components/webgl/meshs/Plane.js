import { Mesh, MeshBasicMaterial, PlaneGeometry, Vector3 } from "three"

export default class Plane extends Mesh {
  constructor(color) {
    const geometry = new PlaneGeometry(1, 1, 1, 1)
    const material = new MeshBasicMaterial({ color })

    super(geometry, material)
  }
  resize(w, h) {
    this.scale.set(w, h, 1.0)
  }
  move(x, y, z) {
    this.position.copy(new Vector3(x, y, z))
  }
}
