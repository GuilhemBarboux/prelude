import React, { useContext, useEffect } from "react"
import PropTypes from "prop-types"
import { Object3D } from "three"
import BackgroundContext from "@src/components/webgl/BackgroundContext"

const BackgroundMesh = ({ mesh }) => {
  const {
    addUpdate,
    removeUpdate,
    addResize,
    removeResize,
    addObject,
    removeObject,
  } = useContext(BackgroundContext)

  useEffect(() => {
    if (mesh.object) addObject(mesh.object)
    if (mesh.resize) addResize(mesh.resize)
    if (mesh.update) addUpdate(mesh.update)

    return () => {
      if (mesh.object) removeObject(mesh.object)
      if (mesh.resize) removeResize(mesh.resize)
      if (mesh.update) removeUpdate(mesh.update)
    }
  }, [mesh])

  return ""
}

BackgroundMesh.propTypes = {
  mesh: PropTypes.shape({
    object: PropTypes.instanceOf(Object3D),
    resize: PropTypes.func,
    update: PropTypes.func,
  }),
}

BackgroundMesh.defaultProps = {
  mesh: PropTypes.shape({
    object: new Object3D(),
    resize: () => {},
    update: () => {},
  }),
}

export default BackgroundMesh
