import React, { useContext, useEffect, useMemo, useRef, useState } from "react"
import { Scene, OrthographicCamera, WebGLRenderer } from "three"
import BackgroundContext from "@src/components/webgl/BackgroundContext"
import PropTypes from "prop-types"
import styled from "@emotion/styled"
import { EffectComposer } from "three/examples/jsm/postprocessing/EffectComposer"
import {
  TransitionPass,
  EstampePass,
  BlurPass,
  HslRgbPass,
  GridPass,
  DeformationPass,
} from "@src/components/webgl/passes"
import { RenderPass } from "three/examples/jsm/postprocessing/RenderPass"

const Canvas = styled.canvas`
  display: block;
  width: 100%;
  height: 100%;
`

const BackgroundCanvas = props => {
  const { transition, deformation, objects, box } = props

  // Context
  const { addResize, removeResize, addUpdate, removeUpdate } = useContext(
    BackgroundContext
  )

  // Refs
  const [scene] = useState(new Scene())
  const canvasRef = useRef()
  const previousObjects = useRef([])
  const [passes, setPasses] = useState({})
  const isTransition = useRef(false)

  // THREE JS
  useEffect(() => {
    const camera = new OrthographicCamera(-1, 1, -1, 1, 0, 1000)
    const renderer = new WebGLRenderer({
      canvas: canvasRef.current,
    })

    // Initialize
    renderer.setClearColor(0xffffff)
    camera.position.z = 500

    // Passes
    const composer = new EffectComposer(renderer)
    const passes = {
      transition: new TransitionPass(),
      estampe: new EstampePass(),
      blur: new BlurPass(),
      hslrgb: new HslRgbPass(),
      grid: new GridPass(),
      deformation: new DeformationPass(),
    }

    composer.addPass(new RenderPass(scene, camera))
    composer.addPass(passes.deformation)
    composer.addPass(passes.transition)
    composer.addPass(passes.estampe)
    composer.addPass(passes.hslrgb)
    composer.addPass(passes.blur)
    composer.addPass(passes.blur)
    composer.addPass(passes.grid)

    setPasses(passes)

    // Resizing
    const resizing = () => {
      const { innerWidth: w, innerHeight: h } = window

      camera.left = w / -2
      camera.right = w / 2
      camera.top = h / 2
      camera.bottom = h / -2
      camera.updateProjectionMatrix()

      // Passes
      passes.estampe.setResolution(w, h)
      passes.blur.setResolution(w, h)
      passes.hslrgb.setResolution(w, h)
      passes.grid.setResolution(w, h)
      passes.deformation.setResolution(w, h)

      renderer.setSize(w, h)
      renderer.setPixelRatio(window.devicePixelRatio)
      composer.setSize(w, h)
    }

    addResize(resizing)

    // Rendering
    const render = () => {
      passes.grid.updateTime()

      composer.render()
    }

    addUpdate(render)

    // Start
    resizing()

    // Cancel
    return () => {
      // Three
      // scene.dispose()
      renderer.renderLists.dispose()
      renderer.dispose()

      // Resizing
      removeResize(resizing)

      // Rendering
      removeUpdate(render)
    }
  }, [scene])

  // Updates
  useEffect(() => {
    previousObjects.current.forEach(o => scene.remove(o))
    objects.forEach(o => scene.add(o))
    previousObjects.current = objects
  }, [objects, scene])

  useEffect(() => {
    if (passes.grid && passes.transition) {
      passes.grid.setBox()
      passes.transition.setTransition(transition)
      isTransition.current = transition < 1 && transition > 0
    }
  }, [passes, transition])

  useEffect(() => {
    if (passes.grid && !isTransition.current) {
      passes.grid.setBox(box)
    }
  }, [passes, box])

  useEffect(() => {
    if (passes.deformation) {
      passes.deformation.addDeformation(deformation)
    }
  }, [passes, deformation])

  // Canvas
  return useMemo(() => <Canvas ref={canvasRef} />, [])
}

BackgroundCanvas.propTypes = {
  objects: PropTypes.array,
  transition: PropTypes.number,
}

BackgroundCanvas.defaultProps = {
  objects: [],
  transition: 0,
}

export default BackgroundCanvas
