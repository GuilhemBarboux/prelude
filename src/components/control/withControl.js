import React from "react"
import ControlManager from "./ControlManager"

const withControl = (Component, workerURL) => props => {
  return (
    <ControlManager workerURL={workerURL}>
      <Component {...props} />
    </ControlManager>
  )
}

export default withControl
