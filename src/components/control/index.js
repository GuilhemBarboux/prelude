import ControlManager, { ControlContext } from "./ControlManager"
import withControl from "./withControl"

export default ControlManager
export { ControlContext, ControlManager, withControl }
