import React, {
  createContext,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react"

export const ControlContext = createContext({
  active: false,
  transition: 0,
  irisRadius: [],
  keyControl: false,
  faceControl: false,
  startKeyControl: () => {},
  stopKeyControl: () => {},
  startFacecontrol: () => {},
  stopFaceControl: () => {},
})

const debugging = false

let config = {
  debug: debugging,
  disabled: debugging,
  threshold: 0.55,
  contrast: 0.4,
  brightness: 0.65,
  limit: 0.1,
}

let debug = () => {}

/* Debug */
if (config.debug && typeof window != "undefined") {
  const debugCanvas = document.createElement("canvas")
  const debugctx = debugCanvas.getContext("2d")

  // Canvas
  debugCanvas.width = 400
  debugCanvas.height = 220
  debugCanvas.style.position = "fixed"
  debugCanvas.style.width = `${debugCanvas.width}px`
  debugCanvas.style.height = `${debugCanvas.height}px`
  debugCanvas.style.top = "0px"
  debugCanvas.style.left = "0px"
  debugCanvas.style.border = "3px solid"
  debugCanvas.style.borderColor = "black"
  document.body.appendChild(debugCanvas)

  // Percents
  let percents = [
    document.createElement("span"),
    document.createElement("span"),
  ]
  percents[0].style.left = "40px"
  percents[1].style.left = "250px"
  percents.forEach(p => {
    p.style.position = "fixed"
    p.style.top = "140px"
    p.style.fontSize = "40px"
    document.body.appendChild(p)
  })

  // GUI
  import("dat.gui").then(dat => {
    const gui = new dat.GUI()
    gui.add(config, "debug")
    gui.add(config, "disabled")
    gui.add(config, "threshold", 0, 1)
    gui.add(config, "contrast", 0, 1)
    gui.add(config, "brightness", 0, 1)
    gui.add(config, "limit", 0, 1)
  })

  debug = result => {
    debugctx.clearRect(0, 0, debugCanvas.width, debugCanvas.height)

    if (Array.isArray(result.thresholds)) {
      result.thresholds.forEach((t, i) => {
        debugctx.putImageData(t, i * 200, 0)
      })
    }

    if (result.irisRadius) {
      percents.forEach((p, side) => {
        percents[side].innerHTML =
          parseFloat(result.irisRadius[side]).toFixed(2) + "%"
        percents[side].style.color =
          result.irisRadius[side] > config.limit ? "red" : "black"
      })
    }

    debugCanvas.style.borderColor =
      result.isOpenEyes && result.isVisage ? "red" : "black"
  }
}

const ControlManager = ({ children, workerURL }) => {
  const [active, setActive] = useState(false)
  const [irisRadius, setIrisRadius] = useState([0, 0])
  const keyLock = useRef(false)
  const eyesLock = useRef(false)

  // Key control
  const [keyControl, setKeyControl] = useState(false)
  useEffect(() => {
    if (keyControl) {
      const keyup = e => {
        if (!eyesLock.current && e.keyCode === 32) {
          keyLock.current = false
          setActive(false)
        }
      }
      const keydown = e => {
        if (!eyesLock.current && e.keyCode === 32) {
          keyLock.current = true
          setActive(true)
        }
      }

      document.body.addEventListener("keydown", keydown)
      document.body.addEventListener("keyup", keyup)

      return () => {
        document.body.removeEventListener("keydown", keydown)
        document.body.removeEventListener("keyup", keyup)
      }
    }
  }, [keyControl])

  const startKeyControl = useCallback(() => setKeyControl(true), [
    setKeyControl,
  ])
  const stopKeyControl = useCallback(() => setKeyControl(false), [
    setKeyControl,
  ])

  // Face tracking section
  const [faceControl, setFaceControl] = useState(false)
  const stream = useRef(null)
  const raf = useRef(null)
  const eyesTo = useRef(null)
  const worker = useRef(null)
  const lock = useRef(false)

  useEffect(() => {
    function onmessage(msg) {
      switch (msg.data.type) {
        case "error":
          console.error(msg.data.error)
      }
    }
    worker.current = new Worker(workerURL)
    worker.current.addEventListener("message", onmessage)
  }, [workerURL])

  const setStream = useCallback(
    s =>
      new Promise(async (resolve, reject) => {
        const canvas = document.createElement("canvas")
        const ctx = canvas.getContext("2d")
        const video = document.createElement("video")

        // Frame interval
        let now = 0
        let then = Date.now()
        let delta
        const interval = 1000 / 2

        // Tracking section
        function processFrame() {
          raf.current = requestAnimationFrame(processFrame)

          now = Date.now()
          delta = now - then

          if (delta > interval && !lock.current) {
            lock.current = true
            then = now - (delta % interval)
            ctx.drawImage(video, 0, 0, canvas.width, canvas.height)
            worker.current.postMessage({
              type: "tracking",
              imageData: ctx.getImageData(0, 0, canvas.width, canvas.height),
              config,
            })
          }
        }

        // Worker section
        function onmessage(msg) {
          switch (msg.data.type) {
            case "ready":
              processFrame()
              break
            case "result":
              resolve(true)
              setFaceControl(true)
              lock.current = false

              if (config.debug) debug(msg.data)

              if (!config.disabled && !keyLock.current) {
                if (!msg.data.isOpenEyes && !eyesTo.current) {
                  eyesLock.current = true
                  eyesTo.current = setTimeout(() => setActive(true), 500)
                } else if (msg.data.isOpenEyes && eyesTo.current) {
                  eyesTo.current = clearTimeout(eyesTo.current)
                  eyesLock.current = false
                  setActive(false)
                }
              }

              // setIrisRadius(msg.data.isVisage ? msg.data.irisRadius : [0, 0])

              break
            case "error":
              clearTimeout(raf.current)
              reject()
          }
        }
        worker.current.addEventListener("message", onmessage)

        // Video section
        video.playsinline = true // Safari
        video.muted = true
        video.srcObject = stream.current = s

        // Start video
        await video.play()

        // Config by video
        canvas.width = video.videoWidth
        canvas.height = video.videoHeight

        worker.current.postMessage({
          type: "init",
          width: canvas.width,
          height: canvas.height,
          debug: config.debug,
        })
      }),
    [setActive, setFaceControl, setIrisRadius]
  )

  // Return true if all is of
  // Return false if refuse webcam
  // Return reject if error
  const startFaceControl = useCallback(
    () =>
      new Promise((resolve, reject) => {
        try {
          if (!stream.current) {
            navigator.mediaDevices
              .getUserMedia({
                video: {
                  width: { max: 720, min: 360, ideal: 720 },
                },
                audio: false,
              })
              .then(
                s => setStream(s),
                () => resolve(false)
              )
              .then(tracking => resolve(tracking))
              .catch(e => reject(e))
          } else {
            resolve(true)
          }
        } catch (e) {
          reject(e)
        }
      }),
    [workerURL, setStream]
  )
  const stopFaceControl = useCallback(() => setFaceControl(false), [])

  return (
    <ControlContext.Provider
      value={{
        active,
        irisRadius,
        keyControl,
        faceControl,
        startKeyControl,
        stopKeyControl,
        startFaceControl,
        stopFaceControl,
      }}
    >
      {children}
    </ControlContext.Provider>
  )
}

export default ControlManager
