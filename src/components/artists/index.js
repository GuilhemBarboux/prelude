import ArtistManager, { ArtistContext } from "./ArtistManager"
import withArtists from "./withArtists"

export default ArtistManager
export { ArtistContext, ArtistManager, withArtists }
