import React, { createContext } from "react"

export const ArtistContext = createContext({
  artists: [],
  current: {},
  nextArtist: () => {},
  stopArtist: () => {},
})

const ArtistManager = ({ children }) => {
  return <ArtistContext.Provider value={{}}>{children}</ArtistContext.Provider>
}

ArtistManager.propTypes = {}

export default ArtistManager
