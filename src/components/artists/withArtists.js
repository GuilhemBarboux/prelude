import React, { useState } from "react"
import ArtistManager from "@src/components/audio/ArtistManager"

const withArtists = Component => props => {
  return (
    <ArtistManager artists={props.artists} listenPath={}>
      <Component {...props} />
    </ArtistManager>
  )
}

export default withArtists
