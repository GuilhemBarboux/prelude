import React, { useContext, useEffect, useMemo, useRef, useState } from "react"
import PropTypes from "prop-types"
import BackgroundMesh from "@src/components/webgl/BackgroundMesh"
import { Object3D, Vector4 } from "three"
import {
  Plane,
  PlaneImage,
  Text,
  TextAnimate,
} from "@src/components/webgl/meshs"
import { BackgroundContext } from "@src/components/webgl"

const Home = ({ logoSrc, startHover, creditsHover }) => {
  const [mesh, setMesh] = useState({})
  const { fonts, setBox } = useContext(BackgroundContext)

  useEffect(() => {
    const object = new Object3D()

    // Fonts
    const titleFont = fonts.title
    const bodyFont = fonts.body

    // Components
    const bottom = new Plane(0x000000)
    const separator = new Plane(0xffffff)
    const logo = new PlaneImage(logoSrc, 0xffffff)
    const title = new Text(titleFont, "PRÉLUDE", 0x000000)
    const cta = new Text(bodyFont, "CLIQUER POUR ENTRER", 0xffffff)
    const explain = new Text(bodyFont, "EXPÉRIENCE EN PLEIN ÉCRAN", 0xffffff)
    const banner = new TextAnimate(
      bodyFont,
      "Projet gobelins réalisé par Clément hazan & charlie le maignan & kevin leiba & guilhem barboux & mathieu débit. avec l’aide de Germain Calsou & Véronique Ficara & samsy & Will & furiouzz.".toUpperCase(),
      0xffffff
    )

    // Add
    object.add(bottom, separator, logo, title, cta, explain, banner)

    // Setter
    const resize = () => {
      const { innerWidth: w, innerHeight: h } = window

      bottom.resize(w, h / 3)
      bottom.move(0, -h / 3, 1)

      separator.resize(w, 2)
      separator.move(0, -h / 2 + h / 9, 2)

      title.resize(120)
      title.move(0, h / 2 / 6, 3)

      cta.resize(30)
      cta.move(0, (-h / 2 / 6) * 3 - 10, 4)

      explain.resize(14)
      explain.move(0, (-h / 2 / 6) * 3 - 50, 4)

      banner.resize(w, 14)
      banner.move(0, -h / 2 + h / 9 / 2, 4)

      logo.resize(148)
      logo.move(0, h / 2 - h / 9, 1)
    }

    const update = () => {
      banner.animate()
    }

    resize()

    setMesh({ object, resize, update })
  }, [setMesh, logoSrc, fonts.title, fonts.body])

  useEffect(() => {
    const { innerWidth: w, innerHeight: h } = window

    if (startHover) {
      setBox(new Vector4(0, h / 9, w, (h * 2) / 9))
    } else if (creditsHover) {
      setBox(new Vector4(0, 0, w, h / 9))
    } else {
      setBox(new Vector4())
    }

    return () => {
      setBox(new Vector4())
    }
  }, [startHover, creditsHover])

  return <BackgroundMesh mesh={mesh} />
}

Home.propTypes = {
  logoSrc: PropTypes.string,
}

Home.defaultProps = {
  logoSrc: "",
}

export default Home
