import React, { useContext, useEffect, useRef, useState } from "react"
import PropTypes from "prop-types"
import BackgroundMesh from "@src/components/webgl/BackgroundMesh"
import { Object3D, Vector4 } from "three"
import { Disclaimer, Plane, Text } from "@src/components/webgl/meshs"
import { BackgroundContext } from "@src/components/webgl"

const ExplainEyes = ({ startHover, eyesValue }) => {
  const [mesh, setMesh] = useState({})
  const { fonts, setBox } = useContext(BackgroundContext)
  const eyes = useRef([])

  useEffect(() => {
    const object = new Object3D()

    if (eyes.current.length === 0) {
      eyes.current = [
        new Disclaimer(Array.isArray(eyesValue)),
        new Disclaimer(Array.isArray(eyesValue)),
      ]
    }

    // Fonts
    const bodyFont = fonts.body
    const titleFont = fonts.title

    // Components
    const xp = new Plane(0x000000)
    const xpText = new Text(bodyFont, "FERME LES YEUX POUR COMMENCER", 0xffffff)
    const xpTextExplain = new Text(
      bodyFont,
      "“ POUR UNE EXPERIENCE OPTIMALE, RETIRE TES LUNETTES “",
      0xffffff
    )

    const explains = [
      new Text(titleFont, "FERME LES YEUX :", 0x000000),
      new Text(titleFont, "IMMERGE TOI DANS UN CONCERT", 0x000000),
      new Text(titleFont, "OUVRE LES :", 0x000000),
      new Text(titleFont, "LORSQUE LA MUSIQUE S'ARRÊTE", 0x000000),
      new Text(titleFont, "POUR REPRODUIRE L'EXPERIENCE :", 0x000000),
      new Text(titleFont, "REFERME LES", 0x000000),
    ]

    explains[0].material.transparent = true
    explains[0].material.opacity = 0.25
    explains[2].material.transparent = true
    explains[2].material.opacity = 0.25
    explains[4].material.transparent = true
    explains[4].material.opacity = 0.25

    // Add
    object.add(xp, xpText, eyes.current[0], eyes.current[1], xpTextExplain)
    explains.forEach(e => object.add(e))

    // Setter
    const resize = () => {
      const { innerWidth: w, innerHeight: h } = window

      xp.resize(w, h / 3)
      xp.move(0, -h / 2 + h / 2 / 3, 1)

      xpText.resize(26)
      xpText.move(0, -h / 2 + h / 6 + 30, 2)

      xpTextExplain.resize(12)
      xpTextExplain.move(0, -h / 2 + h / 2 / 3 - 30, 2)

      const delta =
        (xpText.geometry.boundingBox.max.x -
          xpText.geometry.boundingBox.min.x) /
          2 +
        100

      eyes.current[0].resize(h / 9, h / 9)
      eyes.current[0].move(-delta, -h / 2 + h / 2 / 3, 3)

      eyes.current[1].resize(h / 9, h / 9)
      eyes.current[1].move(delta, -h / 2 + h / 2 / 3, 3)

      const explainSize = 46
      const padding = 30
      explains.map((t, i) => {
        t.resize(explainSize)
        t.move(
          -w / 2 - t.geometry.boundingBox.min.x + padding,
          h / 2 - explainSize / 2 - padding - (h / 10) * i,
          3
        )
      })
    }

    const update = () => {
      eyes.current[0].animate()
      eyes.current[1].animate()
    }

    resize()

    setMesh({ object, resize, update })
  }, [setMesh, fonts.body, fonts.title])

  useEffect(() => {
    const { innerWidth: w, innerHeight: h } = window

    setBox(startHover ? new Vector4(0, 0, w, h / 3) : new Vector4())

    return () => {
      setBox(new Vector4())
    }
  }, [startHover])

  useEffect(() => {
    if (Array.isArray(eyesValue)) {
      eyes.current[0].closure(eyesValue[0])
      eyes.current[1].closure(eyesValue[1])
    }
  }, [eyesValue, eyes])

  return <BackgroundMesh mesh={mesh} />
}

ExplainEyes.propTypes = {
  logoSrc: PropTypes.string,
}

ExplainEyes.defaultProps = {
  logoSrc: "",
}

export default ExplainEyes
