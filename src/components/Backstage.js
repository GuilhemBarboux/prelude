import React, { useContext, useEffect, useMemo, useState } from "react"
import { Object3D, Vector4 } from "three"
import { Plane, Text } from "@src/components/webgl/meshs"
import { BackgroundContext, BackgroundMesh } from "@src/components/webgl"

const Backstage = ({ backHover }) => {
  const [mesh, setMesh] = useState({})
  const { fonts, setBox } = useContext(BackgroundContext)

  useEffect(() => {
    const object = new Object3D()

    // Components
    const back = new Plane(0x000000)
    const separator = new Plane(0x000000)

    const share = new Text(fonts.body, "PARTAGER CETTE EXPÉRIENCE", 0x000000)
    const ticket = new Text(fonts.body, "PRENDRE UN TICKET", 0x000000)
    const backText = new Text(fonts.body, "RETOURNER DANS LA FOSSE", 0xffffff)

    // Add
    object.add(back, separator, share, ticket, backText)

    // Setter
    const resize = () => {
      const { innerWidth: w, innerHeight: h } = window

      back.resize(w, h / 3)
      back.move(0, -h / 2 + h / 6, 1)

      separator.resize(w, 2)
      separator.move(0, h / 2 - h / 3, 1)

      share.resize(30)
      share.move(0, h / 2 - h / 6, 2)

      ticket.resize(30)
      ticket.move(0, h / 2 - (h / 6) * 3, 2)

      backText.resize(30)
      backText.move(0, -h / 2 + h / 6, 2)
    }

    const update = () => {}

    resize()

    setMesh({ object, resize, update })
  }, [fonts.body, setMesh])

  useEffect(() => {
    const { innerWidth: w, innerHeight: h } = window

    setBox(backHover ? new Vector4(0, 0, w, h / 3) : new Vector4())

    return () => {
      setBox(new Vector4())
    }
  }, [backHover])

  return <BackgroundMesh mesh={mesh} />
}

Backstage.propTypes = {}

Backstage.defaultProps = {}

export default Backstage
