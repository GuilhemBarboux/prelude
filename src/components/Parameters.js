import React, { useContext, useEffect, useMemo, useRef, useState } from "react"
import PropTypes from "prop-types"
import BackgroundMesh from "@src/components/webgl/BackgroundMesh"
import { Object3D, Vector4 } from "three"
import {
  PlaneImage,
  Plane,
  Text,
  Disclaimer,
  TextAnimate,
} from "@src/components/webgl/meshs"
import { BackgroundContext } from "@src/components/webgl"

const Parameters = ({
  startHoverValue,
  eyesValue,
  keyControl,
  faceControl,
}) => {
  const [mesh, setMesh] = useState({})
  const { fonts, setBox } = useContext(BackgroundContext)
  const eyes = useRef([])
  const loading = useRef()
  const CTA = useRef([])

  useEffect(() => {
    const object = new Object3D()

    // Eyes
    if (eyes.current.length === 0) {
      eyes.current = [new Disclaimer(true, true), new Disclaimer(true, true)]
    }

    // Components
    const xp = new Plane(0x000000)
    const load = new TextAnimate(
      fonts.body,
      "CHARGEMENT ... CHARGEMENT ... CHARGEMENT ... CHARGEMENT ... CHARGEMENT ... CHARGEMENT ... CHARGEMENT ... CHARGEMENT ...",
      0xffffff
    )
    loading.current = load

    const xpText = new Text(
      fonts.body,
      "FERMER LES YEUX OU MAINTENIR ESPACE",
      0xffffff
    )
    xpText.visible = false
    const xpTextExplain = new Text(fonts.body, "POUR COMMENCER", 0xffffff)
    xpTextExplain.visible = false
    CTA.current = [xpText, xpTextExplain]

    const explains = [
      new Text(fonts.title, "CONTRÔLE LE CONCERT AVEC TA WEBCAM", 0x000000),
      new Text(fonts.title, "FERME LES YEUX, ENTRE DANS LA SALLE", 0x000000),
      new Text(fonts.title, "OUVRE-LES POUR DÉCOUVRIR L'ARTISTE", 0x000000),
    ]

    // Add
    object.add(xp, xpText, xpTextExplain, load)
    explains.forEach(e => object.add(e))
    eyes.current.forEach(e => object.add(e))

    // Setter
    const resize = () => {
      const { innerWidth: w, innerHeight: h } = window

      xp.resize(w, h / 3)
      xp.move(0, -h / 2 + h / 2 / 3, 1)

      load.resize(w, 30)
      load.move(0, -h / 2 + h / 2 / 3, 2)

      xpText.resize(30)
      xpText.move(0, -h / 2 + h / 2 / 3 + 30, 2)

      xpTextExplain.resize(30)
      xpTextExplain.move(0, -h / 2 + h / 2 / 3 - 30, 2)

      const position = h / 2 - h / 3 - 40
      const explainSize = h / 22
      explains.map((t, i) => {
        t.resize(explainSize)
        t.move(0, position - explainSize * i * 2.1, 3)
      })

      const eyeSize = h / 3
      eyes.current[0].resize(eyeSize, eyeSize)
      eyes.current[0].move(
        (-eyeSize * 3) / 4,
        position + eyeSize / 2 + explainSize,
        2
      )

      eyes.current[1].resize(eyeSize, eyeSize)
      eyes.current[1].move(
        (eyeSize * 3) / 4,
        position + eyeSize / 2 + explainSize + 3,
        2
      )
    }

    const update = () => {
      eyes.current[0].animate()
      eyes.current[1].animate()
      load.animate()
    }

    resize()

    setMesh({ object, resize, update })
  }, [setMesh, fonts.body, fonts.title])

  useEffect(() => {
    const { innerWidth: w, innerHeight: h } = window

    setBox(new Vector4(0, 0, w, (startHoverValue * h) / 3))

    return () => {
      setBox(new Vector4())
    }
  }, [startHoverValue])

  useEffect(() => {
    if (eyesValue && eyesValue[0] > 0 && eyesValue[1] > 0) {
      eyes.current[0].closure(eyesValue[0])
      eyes.current[1].closure(eyesValue[1])
    }
  }, [eyesValue, eyes])

  useEffect(() => {
    if (faceControl || keyControl) {
      loading.current.visible = false
      CTA.current.forEach(c => {
        c.visible = true
      })
    }
  }, [faceControl, keyControl])

  return <BackgroundMesh mesh={mesh} />
}

Parameters.propTypes = {
  logoSrc: PropTypes.string,
}

Parameters.defaultProps = {
  logoSrc: "",
}

export default Parameters
