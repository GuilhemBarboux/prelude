import React, { useContext, useEffect, useMemo, useRef, useState } from "react"
import PropTypes from "prop-types"
import BackgroundMesh from "@src/components/webgl/BackgroundMesh"
import { Object3D, Vector4 } from "three"
import {
  Plane,
  PlaneImage,
  Text,
  TextAnimate,
} from "@src/components/webgl/meshs"
import { BackgroundContext } from "@src/components/webgl"

const Credits = ({ logoSrc, startHover }) => {
  const [mesh, setMesh] = useState({})
  const { fonts, setBox } = useContext(BackgroundContext)

  useEffect(() => {
    const object = new Object3D()

    // Components
    const logo = new PlaneImage(logoSrc, 0xffffff)
    const back = new Plane(0x000000)
    const backText = new Text(fonts.body, "RETOUR", 0xffffff)
    const separators = [
      new Plane(0x000000),
      new Plane(0x000000),
      new Plane(0x000000),
      new Plane(0x000000),
      new Plane(0x000000),
      new Plane(0x000000),
    ]
    const teams = [
      new Text(fonts.body, "CLÉMENT HAZAN - Design", 0x000000),
      new Text(fonts.body, "CHARLIE LE MAIGNAN - Design", 0x000000),
      new Text(fonts.body, "GUILHEM BARBOUX - Développement", 0x000000),
      new Text(fonts.body, "MATHIEU DÉBIT - Développement", 0x000000),
      new Text(fonts.body, "KEVIN LEIBA - Développement", 0x000000),
      new Text(fonts.body, "GERMAIN CALSOU - Sound design", 0x000000),
    ]

    // Add
    separators.map(s => object.add(s))
    teams.map(t => object.add(t))
    object.add(back, backText, logo)

    // Setter
    const resize = () => {
      const { innerWidth: w, innerHeight: h } = window

      back.resize(w, h / 9)
      back.move(0, -h / 2 + h / 2 / 9, 2)

      backText.resize(20)
      backText.move(0, -h / 2 + h / 2 / 9, 3)

      separators.map((s, i) => {
        s.resize(w, 2)
        s.move(0, -h / 2 + (h / 9) * 2 + (i * h) / 9, 1)
      })

      teams.map((t, i) => {
        t.resize(20)
        t.move(0, h / 2 - (h / 18) * 5 - (i * h) / 9, 1)
      })

      logo.resize(148)
      logo.move(0, h / 2 - h / 9, 0)
    }

    const update = () => {}

    resize()

    setMesh({ object, resize, update })
  }, [setMesh, logoSrc, fonts.body])

  useEffect(() => {
    const { innerWidth: w, innerHeight: h } = window

    setBox(startHover ? new Vector4(0, 0, w, h / 9) : new Vector4())

    return () => {
      setBox(new Vector4())
    }
  }, [startHover])

  return <BackgroundMesh mesh={mesh} />
}

Credits.propTypes = {
  logoSrc: PropTypes.string,
}

Credits.defaultProps = {
  logoSrc: "",
}

export default Credits
