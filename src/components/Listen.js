import React, { useEffect, useState } from "react"
import { Object3D } from "three"
import { Plane } from "@src/components/webgl/meshs"
import { BackgroundMesh } from "@src/components/webgl"

const Listen = () => {
  const [mesh, setMesh] = useState({})

  useEffect(() => {
    const object = new Object3D()

    // Components
    const listen = new Plane(0x000000)

    const separators = [new Plane(0xffffff), new Plane(0xffffff)]

    // Add
    object.add(listen)
    separators.forEach(s => object.add(s))

    // Setter
    const resize = () => {
      const { innerWidth: w, innerHeight: h } = window

      listen.resize(w * 2, h * 2)

      separators.map((s, i) => {
        s.resize(w * 2, 6)
        s.move(0, -h / 2 + (h / (separators.length + 1)) * (i + 1), 1)
      })
    }

    const update = () => {}

    resize()

    setMesh({ object, resize, update })
  }, [setMesh])

  return <BackgroundMesh mesh={mesh} />
}

Listen.propTypes = {}

Listen.defaultProps = {}

export default Listen
