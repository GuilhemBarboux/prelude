import React from "react"
import PropTypes from "prop-types"
import { TransitionGroup, Transition } from "react-transition-group"

const ReactTransition = props => {
  const { children, location, timeout, transitionStyles } = props

  return (
    <TransitionGroup>
      <Transition
        key={location.pathname}
        timeout={{
          enter: timeout,
          exit: timeout,
        }}
      >
        {status => (
          <div
            style={{
              ...transitionStyles[status],
            }}
          >
            {children}
          </div>
        )}
      </Transition>
    </TransitionGroup>
  )
}

ReactTransition.propTypes = {
  children: PropTypes.element.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }),
  timeout: PropTypes.number,
  transitionStyles: PropTypes.shape({
    entering: PropTypes.object,
    entered: PropTypes.object,
    exiting: PropTypes.object,
  }),
}

ReactTransition.defaultProps = {
  location: {},
  timeout: 250,
  transitionStyles: {
    entering: {
      position: `absolute`,
      opacity: 0,
    },
    entered: {
      transition: `opacity 250ms ease-in-out`,
      opacity: 1,
    },
    exiting: {
      transition: `opacity 250ms ease-in-out`,
      opacity: 0,
    },
  },
}

const withCSSTransition = (Component, config = {}) => props => {
  const { location } = props

  return (
    <ReactTransition
      location={location}
      timeout={config.timeout}
      transitionStyles={config.transitionStyles}
    >
      <Component {...props} />
    </ReactTransition>
  )
}

export default withCSSTransition
