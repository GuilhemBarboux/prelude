import React, { useCallback } from "react"
import { SwitchTransition, Transition } from "react-transition-group"
import anime from "animejs/lib/anime.es.js"

const timeout = 500

const transitionStyles = {
  entering: {
    position: `absolute`,
    opacity: 0,
  },
  entered: {
    transition: `opacity 250ms ease-in-out`,
    opacity: 1,
  },
  exiting: {
    transition: `opacity 250ms ease-in-out`,
    opacity: 0,
  },
}

const withSwitchTransition = (Component, config = {}) => ({
  setTransition = () => {},
  ...props
}) => {
  const { location } = props

  const onExit = useCallback(() => {
    const animation = { x: 0 }

    anime({
      targets: animation,
      x: 0.5,
      easing: "easeInOutCubic",
      duration: timeout,
      update: () => setTransition(animation.x),
    })
  }, [setTransition])

  const onEnter = useCallback(() => {
    const animation = { x: 0.5 }

    anime({
      delay: 200,
      targets: animation,
      x: 1,
      easing: "easeInOutCubic",
      duration: timeout,
      update: () => setTransition(animation.x),
    })
  }, [setTransition])

  return (
    <SwitchTransition>
      <Transition
        key={location.pathname}
        onExit={onExit}
        onEnter={onEnter}
        timeout={timeout + 100}
      >
        {status => (
          <div
            style={{
              ...transitionStyles[status],
            }}
          >
            <Component {...props} />
          </div>
        )}
      </Transition>
    </SwitchTransition>
  )
}

export default withSwitchTransition
