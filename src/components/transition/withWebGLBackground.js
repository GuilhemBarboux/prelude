import React, { useCallback, useContext, useState } from "react"
import Background from "@src/components/webgl"
import AudioContext from "@src/components/audio/AudioContext"

const withWebGLBackground = (Component, config = {}) => props => {
  const [transition, setTransition] = useState(0)
  const [deformation, setDeformation] = useState(0)

  const { getAudioValue } = useContext(AudioContext)

  const update = useCallback(() => {
    setDeformation(getAudioValue())
  }, [setDeformation, getAudioValue])

  return (
    <Background
      transition={transition}
      deformation={deformation}
      update={update}
      fonts={config.fonts || {}}
    >
      <Component
        {...props}
        setTransition={setTransition}
        setDeformation={setDeformation}
      />
    </Background>
  )
}

export default withWebGLBackground
