import React, { useContext, useEffect, useMemo, useState } from "react"
import PropTypes from "prop-types"
import BackgroundMesh from "@src/components/webgl/BackgroundMesh"
import { Object3D, Vector4 } from "three"
import { Disclaimer, Plane, Text } from "@src/components/webgl/meshs"
import { BackgroundContext } from "@src/components/webgl"

const ExplainKeys = ({ startHover, eyesError }) => {
  const [mesh, setMesh] = useState({})
  const { fonts, setBox } = useContext(BackgroundContext)
  const [eyes] = useState([new Disclaimer(true), new Disclaimer(true)])

  useEffect(() => {
    const object = new Object3D()

    // Fonts
    const bodyFont = fonts.body
    const titleFont = fonts.title

    // Components
    const xp = new Plane(0x000000)
    const xpText = new Text(bodyFont, "ENFONCE ESPACE POUR COMMENCER", 0xffffff)
    const xpTextExplain = new Text(bodyFont, `“ ${eyesError} “`, 0xffffff)

    const explains = [
      new Text(titleFont, "RESTE APPUYER SUR ESPACE :", 0x000000),
      new Text(titleFont, "IMMERGE TOI DANS UN CONCERT", 0x000000),
      new Text(titleFont, "RELÂCHE ESPACE :", 0x000000),
      new Text(titleFont, "LORSQUE LA MUSIQUE S'ARRÊTE", 0x000000),
      new Text(titleFont, "POUR REPRODUIRE L'EXPERIENCE :", 0x000000),
      new Text(titleFont, "RAPPUYE SUR ESPACE", 0x000000),
    ]

    explains[0].material.transparent = true
    explains[0].material.opacity = 0.25
    explains[2].material.transparent = true
    explains[2].material.opacity = 0.25
    explains[4].material.transparent = true
    explains[4].material.opacity = 0.25

    // Add
    object.add(xp, xpText, eyes[0], eyes[1], xpTextExplain)
    explains.forEach(e => object.add(e))

    // Setter
    const resize = () => {
      const { innerWidth: w, innerHeight: h } = window

      xp.resize(w, h / 3)
      xp.move(0, -h / 2 + h / 2 / 3, 1)

      xpText.resize(26)
      xpText.move(0, -h / 2 + h / 6, 2)

      xpTextExplain.resize(12)
      xpTextExplain.move(0, -h / 2 + h / 2 / 3 + 45, 2)

      const delta =
        (xpText.geometry.boundingBox.max.x -
          xpText.geometry.boundingBox.min.x) /
          2 +
        100

      eyes[0].resize(h / 9, h / 9)
      eyes[0].move(-delta, -h / 2 + h / 2 / 3, 3)

      eyes[1].resize(h / 9, h / 9)
      eyes[1].move(delta, -h / 2 + h / 2 / 3, 3)

      const explainSize = 46
      const padding = 30
      explains.map((t, i) => {
        t.resize(explainSize)
        t.move(
          -w / 2 - t.geometry.boundingBox.min.x + padding,
          h / 2 - explainSize / 2 - padding - (h / 10) * i,
          3
        )
      })
    }

    const update = () => {
      eyes[0].animate()
      eyes[1].animate()
    }

    resize()

    setMesh({ object, resize, update })
  }, [setMesh, fonts.body, fonts.title])

  useEffect(() => {
    const { innerWidth: w, innerHeight: h } = window

    setBox(startHover ? new Vector4(0, 0, w, h / 3) : new Vector4())

    return () => {
      setBox(new Vector4())
    }
  }, [startHover])

  return <BackgroundMesh mesh={mesh} />
}

ExplainKeys.propTypes = {
  logoSrc: PropTypes.string,
}

ExplainKeys.defaultProps = {
  logoSrc: "",
}

export default ExplainKeys
