import React, { useContext, useEffect, useRef, useState } from "react"
import { Object3D, Vector4 } from "three"
import { Plane, Text, PlaneImage } from "@src/components/webgl/meshs"
import { BackgroundContext, BackgroundMesh } from "@src/components/webgl"

const Artist = ({ cover, name, startHover, backstageHover, transition }) => {
  const [mesh, setMesh] = useState({})
  const eyes = useRef([])
  const { fonts, setBox } = useContext(BackgroundContext)

  useEffect(() => {
    const object = new Object3D()

    if (eyes.current.length === 0) {
      // eyes.current = [new Disclaimer(true, true), new Disclaimer(true, true)]
      eyes.current = [
        new Text(fonts.title, "☛", 0x000000),
        new Text(fonts.title, "☚", 0x000000),
      ]
    }

    // Components
    // const backstage = new Plane(0x000000)
    // const backstageText = new TextAnimate(fonts.body, "BACKSTAGE . ", 0xffffff)
    const cta = new Plane(0x000000)
    const ctaText = new Text(
      fonts.body,
      // "FERME LES YEUX POUR LE PROCHAIN ARTISTE",
      "FERMER LES YEUX OU MAINTENIR ESPACE POUR CONTINUER",
      0xffffff
    )
    const title = new Plane(0xffffff)
    const titleText = new Text(fonts.title, name, 0x000000)
    const logo = new PlaneImage(cover, 0xffffff, false, "top")
    const background = new Plane(0x000000)

    // Add
    object.add(
      // background,
      // backstage,
      // backstageText,
      cta,
      ctaText,
      title,
      titleText,
      logo,
      eyes.current[0],
      eyes.current[1]
    )

    // Setter
    const resize = () => {
      const { innerWidth: w, innerHeight: h } = window

      // backstage.resize(w, h / 9)
      // backstage.move(0, -h / 2 + h / 2 / 9, 2)

      // backstageText.resize(w, 24)
      // backstageText.move(0, -h / 2 + h / 2 / 9, 2)

      cta.resize(w, h / 9)
      cta.move(0, -h / 2 + h / 2 / 9, 2)

      ctaText.resize(24)
      ctaText.move(0, -h / 2 + h / 2 / 9, 2)

      const titleSize = h * 0.14
      title.resize(w * 2, (h / 9) * 2)
      title.move(0, h / 2 - (h / 2 / 9) * 2, 2)

      titleText.resize(titleSize)
      titleText.move(0, h / 2 - (h / 2 / 9) * 2, 2)

      const eyeOffset = titleSize * 0.8
      eyes.current[0].resize(titleSize)
      eyes.current[0].move(-w / 2 + eyeOffset, h / 2 - (h / 2 / 9) * 2, 3)

      eyes.current[1].resize(titleSize)
      eyes.current[1].move(w / 2 - eyeOffset, h / 2 - (h / 2 / 9) * 2, 3)

      logo.resize(w, (h / 9) * 5)
      logo.move(0, h / 2 - (h / 2 / 9) * 4, 1)

      background.resize(w * 2, h * 2)
      background.move(0, 0, 0)
    }

    const update = () => {
      // backstageText.animate()
      // eyes.current[0].animate()
      // eyes.current[1].animate()
    }

    resize()

    setMesh({ object, resize, update })
  }, [cover, name, fonts.body, fonts.title, setMesh])

  useEffect(() => {
    const { innerWidth: w, innerHeight: h } = window

    if (backstageHover) {
      setBox(new Vector4(0, (h * 7) / 9, w, (h / 9) * 2))
    } else {
      setBox(new Vector4(0, 0, w, (transition * h) / 9))
    }

    return () => {
      setBox(new Vector4())
    }
  }, [startHover, backstageHover, transition])

  return <BackgroundMesh mesh={mesh} />
}

Artist.propTypes = {}

Artist.defaultProps = {}

export default Artist
