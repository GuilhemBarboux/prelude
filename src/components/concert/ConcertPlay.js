import React, { useContext, useEffect, useRef, useState } from "react"
import { ConcertContext } from "@src/components/concert/Concert"
import { ControlContext } from "@src/components/control"
import anime from "animejs"

const ConcertPlay = ({ callback, nextArtistSrc, setTransition }) => {
  const { play, pause, prepare } = useContext(ConcertContext)
  const { active } = useContext(ControlContext)
  const isConcert = useRef(false)
  const isPlaying = useRef(false)

  // Transition
  const targets = useRef({ x: 0 })
  const [animation, setAnimation] = useState(null)

  useEffect(() => {
    prepare(nextArtistSrc)

    setAnimation(
      anime({
        targets: targets.current,
        autoplay: false,
        duration: 2800,
        x: 1,
        easing: "linear",
        update: () => {
          setTransition(Math.min(targets.current.x, 1))
        },
        complete: () => {
          isConcert.current = true
          callback()
        },
      })
    )
  }, [nextArtistSrc, setTransition, callback])

  useEffect(() => {
    if (animation && !isConcert.current) {
      if (active && !isPlaying.current) {
        play(nextArtistSrc)
        animation.restart()
        isPlaying.current = true
      } else if (isPlaying.current) {
        pause()
        animation.pause()
        setTransition(0)
        isPlaying.current = false
      }
    }
  }, [animation, active, setTransition])

  return ""
}

ConcertPlay.defaultProps = {
  setTransition: () => {},
}

export default ConcertPlay
