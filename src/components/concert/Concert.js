import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react"
import anime from "animejs"
import ambiantSrc from "@src/assets/sounds/ambiant.mp3"
import eventsSrc from "@src/assets/sounds/events.mp3"
import clapSrc from "@src/assets/sounds/clap0.mp3"
import jackSrc from "@src/assets/sounds/jack.mp3"
import { ControlContext, withControl } from "@src/components/control"
import { withAudio, AudioContext } from "@src/components/audio"
import { withPrefix } from "gatsby"

export const ConcertContext = createContext({
  enter: () => {},
  prepare: () => {},
  play: () => {},
  pause: () => {},
  finish: () => {},
  getAudioValue: () => {},
})

const maxmin = value =>
  value ? Math.max(Math.min(parseFloat(value), 1), 0) : 0

const ambiantVolume = 0.3
const Concert = ({ children }) => {
  const { audioContext, registerTrack, playTrack, getAudioValue } = useContext(
    AudioContext
  )

  const ambiant = useRef([])
  const concert = useRef({})
  const clap = useRef({})
  const jack = useRef({})
  const biquad = useRef(audioContext.createBiquadFilter())

  const timeline = useRef(anime())
  const animation = useRef({
    x: 0,
    ambiant: 0,
    clap: 0,
    artist: 0,
  })

  const isEnter = useRef(false)
  const isConcert = useRef(false)

  // Preload Ambiant sounds
  useEffect(() => {
    ambiant.current = [registerTrack(ambiantSrc), registerTrack(eventsSrc)]
    clap.current = registerTrack(clapSrc)
    jack.current = registerTrack(jackSrc)

    jack.current.audio.playbackRate = 3

    biquad.current.type = "lowpass"
    biquad.current.frequency.value = 10
    biquad.current.gain.value = 25
    biquad.current.detune.value = 0
    biquad.current.Q.value = 0.0001
  }, [registerTrack])

  // Start Ambiant
  const enter = useCallback(async () => {
    // Resume context
    if (!isEnter.current) {
      isEnter.current = true

      ambiant.current = await Promise.all([
        playTrack(ambiantSrc, 0, true),
        playTrack(eventsSrc, 0, true),
      ])

      anime({
        targets: animation.current,
        ambiant: ambiantVolume,
        duration: 4000,
        update: () => {
          ambiant.current.forEach(track => {
            track.audio.volume = maxmin(animation.current.ambiant)
          })
        },
      })
    }
  }, [playTrack])

  // Prepare Artist
  const prepare = useCallback(
    src => {
      registerTrack(src, [biquad.current])
    },
    [registerTrack]
  )

  // Start Artist Concert
  const play = useCallback(
    async src => {
      if (isConcert.current) return

      concert.current = await playTrack(src, 0)
      clap.current = await playTrack(clapSrc, 0, false, 1.5)

      isConcert.current = true

      timeline.current.pause()
      timeline.current = anime
        .timeline({
          targets: animation.current,
          easing: "easeInCubic",
        })
        .add({
          clap: 0.7,
          duration: 500,
          update: () => {
            clap.current.audio.volume = maxmin(animation.current.clap)
          },
        })
        .add(
          {
            clap: 0,
            ambiant: 0,
            duration: 6000,
            update: () => {
              clap.current.audio.volume = maxmin(animation.current.clap)
              ambiant.current.forEach(track => {
                track.audio.volume = maxmin(animation.current.ambiant)
              })
            },
          },
          2000
        )
        .add(
          {
            artist: 1,
            duration: 4000,
            update: () => {
              biquad.current.frequency.value =
                maxmin(animation.current.artist) * 9990 + 10
              concert.current.audio.volume = maxmin(animation.current.artist)
            },
          },
          0
        )
    },
    [playTrack]
  )
  // Pause Before Artist Concert
  const pause = useCallback(() => {
    isConcert.current = false

    timeline.current.pause()
    timeline.current = anime({
      targets: animation.current,
      clap: 0,
      artist: 0,
      ambiant: ambiantVolume,
      duration: 2000,
      easing: "easeOutCubic",
      update: () => {
        biquad.current.frequency.value =
          maxmin(animation.current.artist) * 9990 + 10
        concert.current.audio.volume = maxmin(animation.current.artist)
        clap.current.audio.volume = maxmin(animation.current.clap)
        ambiant.current.forEach(track => {
          track.audio.volume = maxmin(animation.current.ambiant)
        })
      },
    })
  }, [])
  // Finish Artist Concert
  const finish = useCallback(async () => {
    isConcert.current = false

    await enter()

    await playTrack(jackSrc, 1)

    timeline.current.pause()
    timeline.current = anime
      .timeline({
        targets: animation.current,
        easing: "easeOutCubic",
        delay: 1600,
      })
      .add({
        artist: 0,
        clap: 0,
        duration: 800,
        update: () => {
          biquad.current.frequency.value =
            maxmin(animation.current.artist) * 9990 + 10
          concert.current.audio.volume = maxmin(animation.current.artist)
          clap.current.audio.volume = maxmin(animation.current.clap)
        },
      })
      .add(
        {
          ambiant: ambiantVolume,
          duration: 1200,
          update: () => {
            ambiant.current.forEach(track => {
              track.audio.volume = maxmin(animation.current.ambiant)
            })
          },
        },
        500
      )
  }, [])

  return (
    <ConcertContext.Provider
      value={{
        enter,
        prepare,
        play,
        pause,
        finish,
        getAudioValue,
      }}
    >
      {children}
    </ConcertContext.Provider>
  )
}

export default withControl(
  withAudio(Concert),
  withPrefix("tfjs/tracking-worker2.js")
)
