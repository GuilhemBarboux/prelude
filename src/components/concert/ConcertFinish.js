import React, { useContext, useEffect, useRef } from "react"
import { ConcertContext } from "@src/components/concert"
import { ControlContext } from "@src/components/control"

const ConcertFinish = ({ callback }) => {
  const { finish } = useContext(ConcertContext)
  const { active } = useContext(ControlContext)
  const isConcert = useRef(true)

  useEffect(() => {
    if (!active && isConcert.current) {
      isConcert.current = false
      finish()
      setTimeout(() => callback(), 1200)
    }
  }, [active])

  return ""
}

ConcertFinish.propTypes = {}

export default ConcertFinish
