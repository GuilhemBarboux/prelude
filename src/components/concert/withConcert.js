import React from "react"
import Concert from "@src/components/concert/Concert"

const withConcert = Component => props => {
  return (
    <Concert>
      <Component {...props} />
    </Concert>
  )
}

export default withConcert
