import React, { useContext, useEffect, useMemo, useRef, useState } from "react"
import PropTypes from "prop-types"
import BackgroundMesh from "@src/components/webgl/BackgroundMesh"
import { Object3D, Vector4 } from "three"
import { Plane, Text, TextAnimate } from "@src/components/webgl/meshs"
import { BackgroundContext } from "@src/components/webgl"

const NotFound = ({ startHover }) => {
  const [mesh, setMesh] = useState({})

  const { fonts, setBox } = useContext(BackgroundContext)

  useEffect(() => {
    const object = new Object3D()

    // Fonts
    const bodyFont = fonts.body
    const titleFont = fonts.title

    // Components
    const back = new Plane(0x000000)
    const backText = new Text(bodyFont, "RETOUR", 0xffffff)
    const notFounds = [...new Array(8)].map(
      () => new TextAnimate(titleFont, "404", 0x000000)
    )
    const speeds = notFounds.map(() => Math.random() * 5)

    // Add
    object.add(back, backText)
    notFounds.forEach(nf => object.add(nf))

    // Setter
    const resize = () => {
      const { innerWidth: w, innerHeight: h } = window

      back.resize(w, h / 9)
      back.move(0, -h / 2 + h / 2 / 9, 1)

      backText.resize(20)
      backText.move(0, -h / 2 + h / 2 / 9, 2)

      const size = h / 10
      notFounds.forEach((nf, i) => {
        nf.resize(w, size)
        nf.animate((i % 2 == 0 ? -1 : 1) * speeds[i] * 100)
        nf.move(0, h / 2 - size / 2 - (h / 9) * i, 3)
      })
    }

    const update = () => {
      notFounds.forEach((nf, i) => {
        nf.animate(i % 2 == 0 ? -speeds[i] : speeds[i])
      })
    }

    resize()

    setMesh({ object, resize, update })
  }, [setMesh, fonts.title, fonts.body])

  useEffect(() => {
    const { innerWidth: w, innerHeight: h } = window

    setBox(startHover ? new Vector4(0, 0, w, h / 9) : new Vector4())

    return () => {
      setBox(new Vector4())
    }
  }, [startHover])

  return <BackgroundMesh mesh={mesh} />
}

NotFound.propTypes = {
  logoSrc: PropTypes.string,
}

NotFound.defaultProps = {
  logoSrc: "",
}

export default NotFound
