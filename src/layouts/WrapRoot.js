import React from "react"
import "normalize.css"
import { ThemeProvider } from "emotion-theming"
import preset from "@src/theme"
import { css, Global } from "@emotion/core"

const WrapRoot = ({ children }) => {
  return (
    <ThemeProvider theme={preset}>
      <>
        <Global styles={theme => css``} />
        {children}
      </>
    </ThemeProvider>
  )
}

export default WrapRoot
