import React, { useCallback, useContext, useState } from "react"
import bodyFontJSON from "@src/assets/fonts/aceLiftBold"
import titleFontJSON from "@src/assets/fonts/Prhyme"
import Background from "@src/components/webgl"
import PageTransition from "@src/layouts/PageTransition"
import { ConcertContext, withConcert } from "@src/components/concert"

const WrapPage = props => {
  const [transition, setTransition] = useState(0)
  const [deformation, setDeformation] = useState(0)

  const { getAudioValue } = useContext(ConcertContext)

  const update = useCallback(() => {
    setDeformation(getAudioValue())
  }, [setDeformation, getAudioValue])

  return (
    <Background
      transition={transition}
      deformation={deformation}
      update={update}
      fonts={{ body: bodyFontJSON, title: titleFontJSON }}
    >
      <PageTransition setTransition={setTransition} location={props.location}>
        {props.children}
      </PageTransition>
    </Background>
  )
}

export default withConcert(WrapPage)
