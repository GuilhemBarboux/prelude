import React, { useCallback, useRef } from "react"
import { SwitchTransition, Transition } from "react-transition-group"
import anime from "animejs/lib/anime.es.js"
import { withPrefix } from "../../.cache/gatsby-browser-entry"

const timeout = 500

const transitionStyles = {
  entering: {
    position: `absolute`,
    opacity: 0,
  },
  entered: {
    transition: `opacity 250ms ease-in-out`,
    opacity: 1,
  },
  exiting: {
    transition: `opacity 250ms ease-in-out`,
    opacity: 0,
  },
}

const PageTransition = ({ setTransition = () => {}, location, children }) => {
  const animation = useRef({ x: 0 })

  const onExit = useCallback(() => {
    anime({
      targets: animation.current,
      x: 0.5,
      easing: "easeInOutCubic",
      duration: timeout,
      update: () => setTransition(animation.current.x),
    })
  }, [setTransition])

  const onEnter = useCallback(() => {
    anime({
      delay: 200,
      targets: animation.current,
      x: 1,
      easing: "easeInOutCubic",
      duration: timeout,
      update: () => setTransition(animation.current.x),
    })
  }, [setTransition])

  return (
    <SwitchTransition>
      <Transition
        key={location.pathname}
        onExit={onExit}
        onEnter={onEnter}
        timeout={timeout + 100}
      >
        {status => (
          <div
            style={{
              ...transitionStyles[status],
            }}
          >
            {children}
          </div>
        )}
      </Transition>
    </SwitchTransition>
  )
}

export default PageTransition
