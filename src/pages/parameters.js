import React, { useCallback, useContext, useEffect, useState } from "react"
import { graphql, Link, navigate, withPrefix } from "gatsby"
import styled from "@emotion/styled"

import Parameters from "@src/components/Parameters"
import { ControlContext } from "@src/components/control"
import ConcertPlay from "@src/components/concert/ConcertPlay"
import ExplainPage from "@src/pages/explain"

const ExplainLink = styled.a`
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 33.33333333%;
  cursor: pointer;
`

const ParametersPage = ({ data }) => {
  const {
    faceControl,
    keyControl,
    startFaceControl,
    startKeyControl,
    irisRadius,
  } = useContext(ControlContext)

  const [artist] = useState(
    data.allMarkdownRemark.nodes[
      Math.floor(Math.random() * data.allMarkdownRemark.nodes.length)
    ]
  )
  const [transition, setTransition] = useState(0)

  const onListen = useCallback(async () => {
    await navigate("/listen/", {
      state: {
        artist: {
          slug: artist.fields.slug,
          cover: artist.frontmatter.cover,
        },
      },
    })
  }, [artist])

  useEffect(() => {
    startFaceControl()
      .then(() => startKeyControl())
      .catch(() => startKeyControl())
  }, [startFaceControl, startKeyControl])

  return (
    <div>
      <Parameters
        startHoverValue={transition}
        eyesValue={irisRadius}
        faceControl={faceControl}
        keyControl={keyControl}
      />
      <ConcertPlay
        callback={onListen}
        nextArtistSrc={withPrefix(artist.frontmatter.mp3)}
        setTransition={setTransition}
      />
    </div>
  )
}

export default ParametersPage

export const pageQuery = graphql`
  query Parameters {
    allMarkdownRemark {
      nodes {
        frontmatter {
          mp3
          cover
        }
        fields {
          slug
        }
      }
    }
  }
`
