import React, { useCallback, useContext, useEffect, useState } from "react"
import { Link, navigate } from "gatsby"
import styled from "@emotion/styled"

import logoSrc from "@src/assets/images/gobelins.jpg"
import Home from "@src/components/Home"
import { ConcertContext } from "@src/components/concert"
import { ControlContext } from "@src/components/control"

const StartLink = styled.a`
  position: fixed;
  bottom: calc(33.3333333% / 3);
  left: 0;
  width: 100%;
  height: calc(33.3333333% * 2 / 3);
  cursor: pointer;
`

const CreditLink = styled(Link)`
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100%;
  height: calc(100% / 9);
  cursor: pointer;
`

const IndexPage = () => {
  const [startHover, setStartHover] = useState(false)
  const [creditsHover, setCreditsHover] = useState(false)
  const { enter } = useContext(ConcertContext)

  const goParameters = useCallback(() => {
    function toggleFullScreen() {
      if (!document.fullscreenElement) {
        document.documentElement.requestFullscreen()
      } else {
        if (document.exitFullscreen) {
          document.exitFullscreen()
        }
      }
    }

    // toggleFullScreen()

    setTimeout(
      () =>
        Promise.all([enter(), navigate("/parameters/")]).catch(e =>
          console.error(e)
        ),
      400
    )
  }, [])

  return (
    <div>
      <Home
        logoSrc={logoSrc}
        startHover={startHover}
        creditsHover={creditsHover}
      />
      <StartLink
        onMouseOver={() => setStartHover(true)}
        onMouseOut={() => setStartHover(false)}
        onClick={goParameters}
      />
      <CreditLink
        onMouseOver={() => setCreditsHover(true)}
        onMouseOut={() => setCreditsHover(false)}
        to="/credits/"
      />
    </div>
  )
}

export default IndexPage
