import React, { useState } from "react"
import { Link } from "gatsby"
import styled from "@emotion/styled"

import NotFound from "@src/components/NotFound"

const BackLink = styled(Link)`
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100%;
  height: calc(100% / 9);
  cursor: pointer;
`

const NotFoundPage = () => {
  const [startHover, setStartHover] = useState(false)
  return (
    <div>
      <NotFound startHover={startHover} />
      <BackLink
        onMouseMove={() => setStartHover(true)}
        onMouseEnter={() => setStartHover(true)}
        onMouseLeave={() => setStartHover(false)}
        to="/"
      />
    </div>
  )
}

export default NotFoundPage
