import React, { useCallback, useEffect } from "react"
import { navigate, withPrefix } from "gatsby"

import ConcertFinish from "@src/components/concert/ConcertFinish"
import Listen from "@src/components/Listen"

const ListenPage = ({ location }) => {
  const onFinish = useCallback(async () => {
    await navigate(location.state.artist.slug, {
      replace: true,
    })
  }, [location])

  useEffect(() => {
    if (location.state && location.state.artist.cover) {
      fetch(withPrefix(location.state.artist.cover))
    }
  }, [location])

  return (
    <div>
      <ConcertFinish callback={onFinish} />
      <Listen />
    </div>
  )
}

export default ListenPage
