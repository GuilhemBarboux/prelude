import React, { useState } from "react"
import { Link } from "gatsby"
import styled from "@emotion/styled"

import logoSrc from "@src/assets/images/gobelins.jpg"
import Credits from "@src/components/Credits"

const BackLink = styled(Link)`
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100%;
  height: calc(100% / 9);
  cursor: pointer;
`

const CreditsPage = () => {
  const [startHover, setStartHover] = useState(false)
  return (
    <div>
      <Credits logoSrc={logoSrc} startHover={startHover} />
      <BackLink
        onMouseMove={() => setStartHover(true)}
        onMouseEnter={() => setStartHover(true)}
        onMouseLeave={() => setStartHover(false)}
        to="/"
      />
    </div>
  )
}

export default CreditsPage
