import React, { useCallback, useContext, useState } from "react"
import { graphql, navigate, withPrefix } from "gatsby"
import ConcertPlay from "@src/components/concert/ConcertPlay"
import ExplainKeys from "@src/components/ExplainKeys"
import ExplainEyes from "@src/components/ExplainEyes"
import { ControlContext } from "@src/components/control"

const ExplainPage = ({ data, location }) => {
  const { irisRadius } = useContext(ControlContext)
  const [artist] = useState(
    data.allMarkdownRemark.nodes[
      Math.floor(Math.random() * data.allMarkdownRemark.nodes.length)
    ]
  )
  const [eyesError] = useState(
    location.state ? location.state.eyesError : false
  )

  const onListen = useCallback(async () => {
    await navigate("/listen/", {
      state: { url: artist.fields.slug },
    })
  }, [artist])

  return (
    <div>
      {eyesError ? (
        <ExplainKeys eyesError={eyesError} />
      ) : (
        <ExplainEyes eyesValue={irisRadius} />
      )}
      <ConcertPlay
        callback={onListen}
        nextArtistSrc={withPrefix(artist.frontmatter.mp3)}
      />
    </div>
  )
}

export default ExplainPage

export const pageQuery = graphql`
  query Projects {
    allMarkdownRemark {
      nodes {
        frontmatter {
          mp3
        }
        fields {
          slug
        }
      }
    }
  }
`
