import React, { useCallback, useState } from "react"
import { graphql, Link, navigate } from "gatsby"
import styled from "@emotion/styled"
import Backstage from "@src/components/Backstage"

const BackLink = styled(Link)`
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100%;
  height: calc(100% / 3);
  cursor: pointer;
`

const BackstagePage = ({ data, location }) => {
  const [backHover, setBackHoverHover] = useState(false)

  // TODO: add go back version to reduce history stack

  return (
    <div>
      <Backstage backHover={backHover} />
      <BackLink
        onMouseMove={() => setBackHoverHover(true)}
        onMouseEnter={() => setBackHoverHover(true)}
        onMouseLeave={() => setBackHoverHover(false)}
        to={data.markdownRemark.fields.slug}
      />
    </div>
  )
}

export default BackstagePage

export const pageQuery = graphql`
  query BackstageById($id: String!) {
    markdownRemark(id: { eq: $id }) {
      fields {
        slug
      }
      frontmatter {
        name
        cover
      }
    }
  }
`
