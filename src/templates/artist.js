import React, { useCallback, useContext, useEffect, useState } from "react"
import { graphql, Link, navigate, withPrefix } from "gatsby"
import styled from "@emotion/styled"
import Artist from "@src/components/Artist"
import ConcertPlay from "@src/components/concert/ConcertPlay"
import { ControlContext } from "@src/components/control"

const BackstageLink = styled(Link)`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: calc(100% * 2 / 9);
  cursor: pointer;
`

const ArtistPage = ({ data, pageContext }) => {
  const [backstageHover, setBackstageHover] = useState(false)
  const { irisRadius, faceControl } = useContext(ControlContext)
  const [transition, setTransition] = useState(0)

  const onListen = useCallback(async () => {
    await navigate("/listen/", {
      state: {
        artist: {
          slug: pageContext.next.fields.slug,
          cover: pageContext.next.frontmatter.cover,
        },
      },
    })
  }, [])

  return (
    <div>
      <ConcertPlay
        callback={onListen}
        nextArtistSrc={withPrefix(pageContext.next.frontmatter.mp3)}
        setTransition={setTransition}
      />
      <Artist
        cover={withPrefix(data.markdownRemark.frontmatter.cover)}
        name={data.markdownRemark.frontmatter.name}
        backstageHover={backstageHover}
        eyesValue={faceControl ? irisRadius : null}
        transition={transition}
      />
      <BackstageLink
        onMouseMove={() => setBackstageHover(true)}
        onMouseEnter={() => setBackstageHover(true)}
        onMouseLeave={() => setBackstageHover(false)}
        to={data.markdownRemark.fields.slug + "/backstage"}
      />
    </div>
  )
}

export default ArtistPage

export const pageQuery = graphql`
  query ProjectById($id: String!) {
    markdownRemark(id: { eq: $id }) {
      fields {
        slug
      }
      frontmatter {
        name
        cover
      }
    }
  }
`
