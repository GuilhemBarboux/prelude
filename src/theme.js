export default {
    breakpoints: ["40em", "52em", "64em", "90em"],
    fontSizes: {},
    colors: {
        background: "white"
    },
    space: {
        gutter: "20px",
    },
    fonts: {
        body: "sans-serif",
    },
    fontWeights: {},
    lineHeights: {},
    zIndices: {},
}
